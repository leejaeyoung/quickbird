package QB_MainVIewPager.OptionPage;

import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.quickbird.quickbird.R;

import Dialog.SelectList.OnSelectDialogListener;
import Dialog.SelectListDialog;
import SearchLayout.OnSearchLayoutListener;
import SearchLayout.SearchState;

/**
 * Created by KyoungSik on 2017-03-20.
 * 옵션페이지 검색 설정
 */
public class OptionSearchLayout  {


    private final String TAG = "OptionSearchLayout";

    private LinearLayout linearLayout;//검색 조건 레이아웃
    private Activity act;
    private TextView itemText;//선택된 품목을 보여주는 TextView
    private TextView myareaText;//선택된 내주변 항목을 보여주는 TextView

    private OnOptionSearchLayoutListener onOptionSearchLayoutListener;

    public OptionSearchLayout(LinearLayout linearLayout,Activity act){
        Log.d(TAG, "생성자");
        this.act = act;
        this.linearLayout = linearLayout;
        init();
    }

    private void init(){
        itemText = (TextView)linearLayout.findViewById(R.id.searhItemText);
        myareaText = (TextView)linearLayout.findViewById(R.id.myareaText);
        initSeaerchState();//전체.화물 및 배송자 선택 버튼 이벤트
        itemButtonEvent();//품목 버튼 이벤트
        myareaButtonEvent();//내주변 버튼 이벤트
    }

    /* 전체.화물 및 배송자 선택 버튼 이벤트
    *
    * */
    private void initSeaerchState(){
        SearchState searchState = new SearchState(linearLayout,act) {
            @Override
            public void onClickButton(int state, SearchState searchState) {
                if(searchState.SEARCH_ALL == state){
                    Log.d(TAG,"SEARCH_ALL");
                }else if(searchState.SEARCH_FREIGHT == state){
                    Log.d(TAG,"SEARCH_FREIGHT");
                }else if(searchState.SEARCH_DELIVERY == state){
                    Log.d(TAG,"SEARCH_DELIVERY");
                }

                if(onOptionSearchLayoutListener != null)
                    onOptionSearchLayoutListener.onClickSearchState(state);

            }
        };
    }

    /*품목 선택 버튼
    *
    * */
    private void itemButtonEvent(){

        Button itembtn = (Button)linearLayout.findViewById(R.id.searchItembtn);
        itembtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG,"onClick");
                String[] titlelist = new String[10];
                for (int i = 0; i < titlelist.length; i++) {
                    titlelist[i] = "itemtest" + i;
                }
                //품목 선택 다이얼로그
                SelectListDialog selectListDialog = new SelectListDialog(act, titlelist);
                selectListDialog.setOnSelectDialogListener(new OnSelectDialogListener() {
                    @Override
                    public void onClickCancel() {

                    }

                    @Override
                    public void onClickConfirm(SelectListDialog selectListDialog, int position, String[] titlelist) {
                        itemText.setText(titlelist[position]);
                        if(onOptionSearchLayoutListener != null)
                            onOptionSearchLayoutListener.onClickItem(position,titlelist);
                        selectListDialog.dismiss();
                    }

                    @Override
                    public void onClickList(int position, View view, String[] titlelist) {
                        //  view.setBackgroundColor(Color.BLUE);
                        Log.d(TAG, "position : " + titlelist[position]);
                    }
                });
                selectListDialog.getTitleText().setText("품목 검색");
                selectListDialog.show();
            }
        });

    }
    /*내주변 선택 버튼 이벤트
    *
    * */
    private void myareaButtonEvent(){
        Button myareabtn = (Button)linearLayout.findViewById(R.id.myareabtn);
        myareabtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG,"onClick");
                String[] titlelist = new String[10];
                for (int i = 0; i < titlelist.length; i++) {
                    titlelist[i] = "myareatest" + i;
                }
                //내주변 선택 다이얼로그
                SelectListDialog selectListDialog = new SelectListDialog(act, titlelist);
                selectListDialog.setOnSelectDialogListener(new OnSelectDialogListener() {
                    @Override
                    public void onClickCancel() {

                    }

                    @Override
                    public void onClickConfirm(SelectListDialog selectListDialog, int position, String[] titlelist) {
                        myareaText.setText(titlelist[position]);
                        if(onOptionSearchLayoutListener != null)
                            onOptionSearchLayoutListener.onClickItem(position,titlelist);
                        selectListDialog.dismiss();
                    }

                    @Override
                    public void onClickList(int position, View view, String[] titlelist) {
                        //  view.setBackgroundColor(Color.BLUE);
                        Log.d(TAG, "position : " + titlelist[position]);
                    }
                });
                selectListDialog.getTitleText().setText("내주변 검색");
                selectListDialog.show();
            }
        });
    }

    public void setOnOptionSearchLayoutListener(OnOptionSearchLayoutListener onOptionSearchLayoutListener) {
        this.onOptionSearchLayoutListener = onOptionSearchLayoutListener;
    }
}
