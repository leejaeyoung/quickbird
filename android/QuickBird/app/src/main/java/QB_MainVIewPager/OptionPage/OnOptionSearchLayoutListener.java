package QB_MainVIewPager.OptionPage;

/**
 * Created by KyoungSik on 2017-03-20.
 * 옵션 검색 설정 이벤트
 */
public interface OnOptionSearchLayoutListener {
    public void onClickSearchState(int state);
    public void onClickItem(int position, String[] titlelist);
}
