package QB_MainVIewPager.HomePage;

import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;

import connection.OnWebViewClientListener;
import connection.WebViewClientClass;

/**
 * Created by KyoungSik on 2017-03-14.
 * 홈 페이지의 웹뷰
 */
public class HomePageWebView {

    private final String TAG = "HomePageWebView";

    private WebView webView;

    private WebViewClientClass webViewClientClass;

    private boolean webLoadFinish = false;

    public HomePageWebView(WebView webView, Activity act, String url){
        this.webView = webView;
        init(act, url);
    }

    private void init(Activity act, String url){
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl(url);
        webViewClientClass = new WebViewClientClass(act);
        webView.setWebViewClient(webViewClientClass);
        webView.setVerticalScrollBarEnabled(false);
        //webView.clearCache(true);
       // webView.getSettings().setAppCacheEnabled(false);
       // webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);

        webView.addJavascriptInterface(new Object(){
            @JavascriptInterface
            public void more_m(){
                Log.d(TAG, "more_m");
            }

            @JavascriptInterface
            public void qhlist(){
                Log.d(TAG,"h_list");
            }
        },"quickbird");
    }


    public WebViewClientClass getWebViewClientClass() {
        return webViewClientClass;
    }

    public boolean isWebLoadFinish() {
        return webLoadFinish;
    }

    public void setWebLoadFinish(boolean webLoadFinish) {
        this.webLoadFinish = webLoadFinish;
    }

    public WebView getWebView() {
        return webView;
    }
}
