package com.quickbird.quickbird;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.widget.Button;

import Dialog.Loading;
import Dialog.SelectThreeDialog;
import Dialog.SelectTwoDialog;
import WebView.WebViewClass;
import connection.OnWebViewClientListener;
import memory.RecycleUtils;

/**
 * Created by KyoungSik on 2017-03-18.
 * 화물 배송을 요청한 배송자 리스트
 */
public class FrequestDeliveryActivity extends Activity {

    private final String TAG = "FrequestDeliveryActivity";

    private WebViewClass webViewClass;
    private Loading loading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_frequest_delivery);
        loading = new Loading(getFrequestDeliveryActivity());
        loading.show();
        init();


    }

    private void init(){
        String urlStr = "http://quickbird.dev.wyhil.com/webview/wisher.php";
        webViewClass = new WebViewClass((WebView)findViewById(R.id.frdwebview),this,urlStr);
        webViewClass.getWebViewClientClass().setOnWebViewClientListener(new OnWebViewClientListener() {
            @Override
            public void startWebClient(WebView webView, String url) {

            }

            @Override
            public void finishWebClient(WebView webView, String url) {
                loading.dismiss();
            }

            @Override
            public void errorWebClient(WebView webView, int errorCode, String failingUrl) {

            }
        });

        webViewClass.getWebView().addJavascriptInterface(new Object() {
            @JavascriptInterface
            public void more_m(int state) {
                showStateDialog(state);
            }

            @JavascriptInterface
            public void h_list(int state) {
                Intent intent = new Intent(FrequestDeliveryActivity.this, DeliveryInfoDetailActivity.class);
                startActivity(intent);
            }
        }, "quickbird");

        Button exitbtn = (Button)findViewById(R.id.frdexitbtn);
        exitbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    /* 배송 상태에 따라 띄우는 다이얼로그 함수
*
* */
    private void showStateDialog(int state){
        switch (state){
            case 1://미확정
                SelectTwoDialog selectTwoDialog = new SelectTwoDialog(getFrequestDeliveryActivity()) {
                    @Override
                    public void clickOneButton(SelectTwoDialog selectTwoDialog) {

                    }

                    @Override
                    public void clickTwoButton(SelectTwoDialog selectTwoDialog) {

                    }

                    @Override
                    public void clickCancelButton() {

                    }
                };
                selectTwoDialog.getSelectOneButton().setText("전화걸기");
                selectTwoDialog.getSelectTwoButton().setText("배송자지정");
                selectTwoDialog.show();
                break;
        }
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        RecycleUtils.recursiveRecycle(getWindow().getDecorView());
    }

    private FrequestDeliveryActivity getFrequestDeliveryActivity(){
        return this;
    }
}
