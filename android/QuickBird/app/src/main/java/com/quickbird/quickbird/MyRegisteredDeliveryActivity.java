package com.quickbird.quickbird;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;

import Dialog.Loading;
import Dialog.SelectFourDialog;
import Dialog.SelectOneDialog;
import Dialog.SelectThreeDialog;
import WebView.WebViewClass;
import connection.OnWebViewClientListener;
import memory.RecycleUtils;

/**
 * Created by KyoungSik on 2017-03-15.
 * 내가 등록한 배송자
 */
public class MyRegisteredDeliveryActivity extends Activity {

    private final String TAG = "MyRegisteredDeliveryActivity";

    private WebViewClass webViewClass;
    private Loading loading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_myregistered_delivery);
        loading = new Loading(getMyRegisteredDeliveryActivity());
        loading.show();
        init();
    }

    //내가 등록한 배송자 웹뷰 셋팅
    private void init(){
        String urlStr = "http://quickbird.dev.wyhil.com/webview/wisher.php";
        webViewClass = new WebViewClass((WebView)findViewById(R.id.mrdwebview),this,urlStr);
        webViewClass.getWebViewClientClass().setOnWebViewClientListener(new OnWebViewClientListener() {
            @Override
            public void startWebClient(WebView webView, String url) {

            }

            @Override
            public void finishWebClient(WebView webView, String url) {
                loading.dismiss();
            }

            @Override
            public void errorWebClient(WebView webView, int errorCode, String failingUrl) {

            }
        });

        webViewClass.getWebView().addJavascriptInterface(new Object() {
            @JavascriptInterface
            public void more_m(int state) {
                showStateDialog(state);
            }

            @JavascriptInterface
            public void h_list() {
                Intent intent = new Intent(MyRegisteredDeliveryActivity.this, DeliveryInfoDetailActivity.class);
                startActivity(intent);
            }
        }, "quickbird");

    }
    /* 배송 상태에 따라 띄우는 다이얼로그 함수
   *
   * */
    private void showStateDialog(int state){
        switch (state){
            case 0://배송확정
                 SelectOneDialog selectOneDialog = new SelectOneDialog(this) {
                    @Override
                    public void clickOneButton(SelectOneDialog selectOneDialog) {
                        Intent intent = new Intent(MyRegisteredDeliveryActivity.this, FreightInfoDetailActivity.class);
                        startActivity(intent);
                        selectOneDialog.dismiss();
                    }

                    @Override
                    public void clickCancelButton() {

                    }
                };
                selectOneDialog.getSelectOneButton().setText("배송중인 화물정보");
                selectOneDialog.show();
                break;
            case 1://미확정
                SelectFourDialog selectFourDialog = new SelectFourDialog(getMyRegisteredDeliveryActivity()) {
                    @Override
                    public void clickOneButton(SelectFourDialog selectFourDialog) {
                        Intent intent = new Intent(MyRegisteredDeliveryActivity.this, DeliveryEditActivity.class);
                        startActivity(intent);
                        selectFourDialog.dismiss();
                    }

                    @Override
                    public void clickTwoButton(SelectFourDialog selectFourDialog) {

                    }

                    @Override
                    public void clickThreeButton(SelectFourDialog selectFourDialog) {
                        Intent intent = new Intent(MyRegisteredDeliveryActivity.this, DrequestFreightActivity.class);
                        startActivity(intent);
                        selectFourDialog.dismiss();
                    }

                    @Override
                    public void clickFourButton(SelectFourDialog selectFourDialog) {

                    }

                    @Override
                    public void clickCancelButton() {

                    }
                };
                selectFourDialog.getSelectOneButton().setText("수정");
                selectFourDialog.getSelectTwoButton().setText("삭제");
                selectFourDialog.getSelectThreeButton().setText("나에게 배송 요청한 화물");
                selectFourDialog.getSelectFourButton().setText("내 배송자로 지정");
                selectFourDialog.show();
                break;

        }
    }

    private MyRegisteredDeliveryActivity getMyRegisteredDeliveryActivity(){
        return this;
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        RecycleUtils.recursiveRecycle(getWindow().getDecorView());
    }
}
