package com.quickbird.quickbird;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.widget.Button;

import Dialog.Loading;
import Dialog.SelectTwoDialog;
import WebView.WebViewClass;
import connection.OnWebViewClientListener;
import memory.RecycleUtils;

/**
 * Created by KyoungSik on 2017-03-20.
 * 화물 관제
 */
public class FreightManagementActivity extends Activity {

    private final String TAG = "FreightManagementActivity";

    private WebViewClass webViewClass;
    private Loading loading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_freight_menagement);
        loading = new Loading(getFreightManagementActivity());
        loading.show();
        init();


    }

    private void init(){
        String urlStr = "http://quickbird.dev.wyhil.com/webview/map.php";
        webViewClass = new WebViewClass((WebView)findViewById(R.id.fmwebview),this,urlStr);
        webViewClass.getWebViewClientClass().setOnWebViewClientListener(new OnWebViewClientListener() {
            @Override
            public void startWebClient(WebView webView, String url) {

            }

            @Override
            public void finishWebClient(WebView webView, String url) {
                loading.dismiss();
            }

            @Override
            public void errorWebClient(WebView webView, int errorCode, String failingUrl) {

            }
        });

        Button exitbtn = (Button)findViewById(R.id.fmexitbtn);
        exitbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        RecycleUtils.recursiveRecycle(getWindow().getDecorView());
    }

    private FreightManagementActivity getFreightManagementActivity(){
        return this;
    }
}
