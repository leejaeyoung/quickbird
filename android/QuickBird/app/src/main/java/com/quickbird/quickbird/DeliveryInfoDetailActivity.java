package com.quickbird.quickbird;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;

import Dialog.Loading;
import WebView.WebViewClass;
import connection.OnWebViewClientListener;
import memory.RecycleUtils;

/**
 * Created by KyoungSik on 2017-03-18.
 * 배송자 상세정보
 */
public class DeliveryInfoDetailActivity extends Activity {

    private final String TAG = "DeliveryInfoDetail";

    private WebViewClass webViewClass;
    private Loading loading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deliveryinfo_detail);
        loading = new Loading(this);
        loading.show();
        init();
    }

    private void init(){
        String urlStr = "http://quickbird.dev.wyhil.com/webview/delivery_detail.php";
        webViewClass = new WebViewClass((WebView)findViewById(R.id.ddwebview),this,urlStr);
        webViewClass.getWebViewClientClass().setOnWebViewClientListener(new OnWebViewClientListener() {
            @Override
            public void startWebClient(WebView webView, String url) {

            }

            @Override
            public void finishWebClient(WebView webView, String url) {
                loading.dismiss();
            }

            @Override
            public void errorWebClient(WebView webView, int errorCode, String failingUrl) {

            }
        });

        webViewClass.getWebView().addJavascriptInterface(new Object(){
            @JavascriptInterface
            public void more_m(){
                Log.d(TAG, "more_m");
            }

            @JavascriptInterface
            public void qhlist(){
                Log.d(TAG,"h_list");
            }
        },"quickbird");
    }


    @Override
    protected void onDestroy(){
        super.onDestroy();
        RecycleUtils.recursiveRecycle(getWindow().getDecorView());
    }

}
