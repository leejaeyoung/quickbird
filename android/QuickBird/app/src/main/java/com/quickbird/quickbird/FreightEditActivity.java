package com.quickbird.quickbird;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import java.util.ArrayList;

import Dialog.ConfirmDialog;
import Dialog.SelectTwoDialog;
import Register.ImageIcon;
import memory.RecycleUtils;

/**
 * Created by KyoungSik on 2017-03-18.
 * 화물 정보 수정하기
 */
public class FreightEditActivity extends Activity {

    private final String TAG = "FreightEditActivity";

    private ArrayList<ImageIcon> imageIcons = new ArrayList<ImageIcon>();//이미지 아이콘 관리

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_freightinfo_edit);
        init();
    }

    private void init(){
        imageIconinit();
        buttonEvent();
        editTextEvent();
    }

    /* 버튼 이벤트
  *
  * */
    private void buttonEvent(){
        Button cancelbtn = (Button)findViewById(R.id.fecancelbtn);
        cancelbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConfirmDialog confirmDialog = new ConfirmDialog(getFreighEditActivity()) {
                    @Override
                    public void onClickConfirm(ConfirmDialog confirmDialog) {
                        confirmDialog.dismiss();
                        finish();
                    }

                    @Override
                    public void onClickCancel() {

                    }
                };
                confirmDialog.getConfirmBtn().setText("확인");
                confirmDialog.getCancelBtn().setText("취소");
                confirmDialog.getTitleText().setText("정말로 취소를 하시겠습니까?");
                confirmDialog.show();
            }
        });

        Button exitbtn = (Button)findViewById(R.id.feexitbtn);
        exitbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Button picturemovebtn = (Button)findViewById(R.id.fePicturemovebtn);
        picturemovebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SelectTwoDialog selectTwoDialog = new SelectTwoDialog(getFreighEditActivity()) {
                    @Override
                    public void clickOneButton(SelectTwoDialog selectTwoDialog) {

                    }

                    @Override
                    public void clickTwoButton(SelectTwoDialog selectTwoDialog) {

                    }

                    @Override
                    public void clickCancelButton() {

                    }
                };
                selectTwoDialog.getSelectOneButton().setText("이미지");
                selectTwoDialog.getSelectTwoButton().setText("동영상");
                selectTwoDialog.show();
            }
        });
    }


    private void imageIconinit(){
        //품목 아이콘
        ImageIcon itemIcon = new ImageIcon();
        itemIcon.setEditText((EditText) findViewById(R.id.feItemText));
        itemIcon.setView((ImageView) findViewById(R.id.feitemImage));
        itemIcon.setSetOnImage(R.drawable.icon_tag_on);
        itemIcon.setSetOffImage(R.drawable.icon_tag_off);
        imageIcons.add(itemIcon);

        //크기 아이콘
        ImageIcon sizeIcon = new ImageIcon();
        sizeIcon.setEditText((EditText) findViewById(R.id.feSizeText));
        sizeIcon.setView((ImageView) findViewById(R.id.fesizeImage));
        sizeIcon.setSetOnImage(R.drawable.icon_width_on);
        sizeIcon.setSetOffImage(R.drawable.icon_width_off);
        imageIcons.add(sizeIcon);

        //무게 아이콘
        ImageIcon weightIcon = new ImageIcon();
        weightIcon.setEditText((EditText) findViewById(R.id.feweightText));
        weightIcon.setView((ImageView) findViewById(R.id.feweightImage));
        weightIcon.setSetOnImage(R.drawable.icon_weight_on);
        weightIcon.setSetOffImage(R.drawable.icon_weight_off);
        imageIcons.add(weightIcon);

        //받는분 아이콘
        ImageIcon reciveIcon = new ImageIcon();
        reciveIcon.setEditText((EditText) findViewById(R.id.fereciveText));
        reciveIcon.setView((ImageView) findViewById(R.id.fereciveImage));
        reciveIcon.setSetOnImage(R.drawable.icon_user_on);
        reciveIcon.setSetOffImage(R.drawable.icon_user_off);
        imageIcons.add(reciveIcon);

        //출발지 아이콘
        ImageIcon dpIcon = new ImageIcon();
        dpIcon.setEditText((EditText) findViewById(R.id.feDeparturePointText));
        dpIcon.setView((ImageView) findViewById(R.id.feDeparturePointImage));
        dpIcon.setSetOnImage(R.drawable.icon_map_on);
        dpIcon.setSetOffImage(R.drawable.icon_map_off);
        imageIcons.add(dpIcon);

        //도착지 아이콘
        ImageIcon apIcon = new ImageIcon();
        apIcon.setEditText((EditText) findViewById(R.id.feArrivePointText));
        apIcon.setView((ImageView) findViewById(R.id.feArrivePointImage));
        apIcon.setSetOnImage(R.drawable.icon_map_on);
        apIcon.setSetOffImage(R.drawable.icon_map_off);
        imageIcons.add(apIcon);

        //예상거리 아이콘
        ImageIcon distanceIcon = new ImageIcon();
        distanceIcon.setEditText((EditText) findViewById(R.id.feDistanceText));
        distanceIcon.setView((ImageView) findViewById(R.id.feDistanceImage));
        distanceIcon.setSetOnImage(R.drawable.icon_map_on);
        distanceIcon.setSetOffImage(R.drawable.icon_map_off);
        imageIcons.add(distanceIcon);

        //추천가격 아이콘
        ImageIcon recommendIcon = new ImageIcon();
        recommendIcon.setEditText((EditText) findViewById(R.id.feRecommendText));
        recommendIcon.setView((ImageView) findViewById(R.id.feRecommendImage));
        recommendIcon.setSetOnImage(R.drawable.icon_card_on);
        recommendIcon.setSetOffImage(R.drawable.icon_card_off);
        imageIcons.add(recommendIcon);

        //메모 아이콘
        ImageIcon memoIcon = new ImageIcon();
        memoIcon.setEditText((EditText) findViewById(R.id.feMemoText));
        memoIcon.setView((ImageView) findViewById(R.id.feMemoImage));
        memoIcon.setSetOnImage(R.drawable.icon_pencil_on);
        memoIcon.setSetOffImage(R.drawable.icon_pencil_off);
        imageIcons.add(memoIcon);
    }

    /* EditText 포커스 바꼈을시 아이콘 이미지 상태 바꿈
        *
        * */
    private void editTextEvent(){

        for(int i=0;i<imageIcons.size();i++) {
            imageIcons.get(i).getEditText().setId(i);
            imageIcons.get(i).getEditText().setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (hasFocus) {
                        imageIcons.get(v.getId()).getView().setBackgroundResource(imageIcons.get(v.getId()).getSetOnImage());
                    } else {
                        imageIcons.get(v.getId()).getView().setBackgroundResource(imageIcons.get(v.getId()).getSetOffImage());
                    }
                }
            });
        }
    }

    private FreightEditActivity getFreighEditActivity(){
        return this;
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        RecycleUtils.recursiveRecycle(getWindow().getDecorView());
    }
}
