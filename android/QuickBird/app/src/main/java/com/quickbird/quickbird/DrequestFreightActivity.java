package com.quickbird.quickbird;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.widget.Button;

import Dialog.Loading;
import Dialog.SelectThreeDialog;
import Dialog.SelectTwoDialog;
import WebView.WebViewClass;
import connection.OnWebViewClientListener;
import memory.RecycleUtils;

/**
 * Created by KyoungSik on 2017-03-18.
 * 배송을 요청한 화물
 */
public class DrequestFreightActivity extends Activity {

    private final String TAG = "DrequestFreightActivity";

    private WebViewClass webViewClass;
    private Loading loading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drequest_freight);
        loading = new Loading(getDrequestFreightActivity());
        loading.show();
        init();


    }

    private void init(){
        String urlStr = "http://quickbird.dev.wyhil.com/webview/mystuff.php";
        webViewClass = new WebViewClass((WebView)findViewById(R.id.drfwebview),this,urlStr);
        webViewClass.getWebViewClientClass().setOnWebViewClientListener(new OnWebViewClientListener() {
            @Override
            public void startWebClient(WebView webView, String url) {

            }

            @Override
            public void finishWebClient(WebView webView, String url) {
                loading.dismiss();
            }

            @Override
            public void errorWebClient(WebView webView, int errorCode, String failingUrl) {

            }
        });

        webViewClass.getWebView().addJavascriptInterface(new Object() {
            @JavascriptInterface
            public void more_m(int state) {
                showStateDialog(state);

            }

            @JavascriptInterface
            public void h_list(int state) {
                Intent intent = new Intent(DrequestFreightActivity.this, FreightInfoDetailActivity.class);
                startActivity(intent);
            }
        }, "quickbird");

        Button exitbtn = (Button)findViewById(R.id.drfexitbtn);
        exitbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    /* 배송 상태에 따라 띄우는 다이얼로그 함수
*
* */
    private void showStateDialog(int state){
        switch (state){
            case 0://배송대기
                SelectTwoDialog selectTwoDialog = new SelectTwoDialog(getDrequestFreightActivity()) {
                    @Override
                    public void clickOneButton(SelectTwoDialog selectTwoDialog) {

                    }

                    @Override
                    public void clickTwoButton(SelectTwoDialog selectTwoDialog) {

                    }

                    @Override
                    public void clickCancelButton() {

                    }
                };
                selectTwoDialog.getSelectOneButton().setText("전화걸기");
                selectTwoDialog.getSelectTwoButton().setText("화물지정");
                selectTwoDialog.show();
                break;
        }
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        RecycleUtils.recursiveRecycle(getWindow().getDecorView());
    }

    private DrequestFreightActivity getDrequestFreightActivity(){
        return this;
    }

}
