package com.quickbird.quickbird;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;

import Dialog.Loading;
import WebView.WebViewClass;
import connection.OnWebViewClientListener;
import memory.RecycleUtils;

/**
 * Created by KyoungSik on 2017-03-18.
 * 화물 상세보기
 */
public class FreightInfoDetailActivity extends Activity {

    private final String TAG = "FreightInfoDetail";

    private WebViewClass webViewClass;
    private Loading loading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_freightinfo_detail);
        loading = new Loading(this);
        loading.show();
        init();
    }

    private void init(){
        String urlStr = "http://quickbird.dev.wyhil.com/webview/stuff_detail.php";
        webViewClass = new WebViewClass((WebView)findViewById(R.id.fdwebview),this,urlStr);
        webViewClass.getWebViewClientClass().setOnWebViewClientListener(new OnWebViewClientListener() {
            @Override
            public void startWebClient(WebView webView, String url) {

            }

            @Override
            public void finishWebClient(WebView webView, String url) {
                loading.dismiss();
            }

            @Override
            public void errorWebClient(WebView webView, int errorCode, String failingUrl) {

            }
        });
    }


    @Override
    protected void onDestroy(){
        super.onDestroy();
        RecycleUtils.recursiveRecycle(getWindow().getDecorView());
    }


}
