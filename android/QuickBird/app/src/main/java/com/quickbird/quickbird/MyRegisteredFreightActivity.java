package com.quickbird.quickbird;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;

import Dialog.DeliveryAssessDialog;
import Dialog.Loading;
import Dialog.SelectFourDialog;
import Dialog.SelectThreeDialog;
import WebView.WebViewClass;
import connection.OnWebViewClientListener;
import memory.RecycleUtils;

/**
 * Created by KyoungSik on 2017-03-15.
 * 내가 등록한 화물
 */
public class MyRegisteredFreightActivity extends Activity {

    private final String TAG = "MyRegisteredFreight";

    private WebViewClass webViewClass;
    private Loading loading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_myregistered_freight);
        loading = new Loading(getMyRegisteredFreightActivity());
        loading.show();
        init();


    }
    
    //내가 등록한 화물 웹뷰 셋팅
    private void init(){
        String urlStr = "http://quickbird.dev.wyhil.com/webview/mystuff.php";
        webViewClass = new WebViewClass((WebView)findViewById(R.id.mrfwebview),this,urlStr);
        webViewClass.getWebViewClientClass().setOnWebViewClientListener(new OnWebViewClientListener() {
            @Override
            public void startWebClient(WebView webView, String url) {

            }

            @Override
            public void finishWebClient(WebView webView, String url) {
                loading.dismiss();
            }

            @Override
            public void errorWebClient(WebView webView, int errorCode, String failingUrl) {

            }
        });

        webViewClass.getWebView().addJavascriptInterface(new Object() {
            @JavascriptInterface
            public void more_m(int state) {
                Log.d(TAG, "idx : " + state);
                showStateDialog(state);
            }

            @JavascriptInterface
            public void h_list(int state) {
                if(state == 0){//화물
                    Intent intent = new Intent(MyRegisteredFreightActivity.this, FreightInfoDetailActivity.class);
                    startActivity(intent);
                }else if(state == 1){//배송자
                    Intent intent = new Intent(MyRegisteredFreightActivity.this, DeliveryInfoDetailActivity.class);
                    startActivity(intent);
                }
            }
        }, "quickbird");

    }

    /* 배송 상태에 따라 띄우는 다이얼로그 함수
    *
    * */
    private void showStateDialog(int state){
        switch (state){
            case 0://배송대기
                SelectThreeDialog deliverybefore = new SelectThreeDialog(getMyRegisteredFreightActivity()) {
                    @Override
                    public void clickOneButton(SelectThreeDialog selectThreeDialog) {
                        Intent intent = new Intent(MyRegisteredFreightActivity.this, FreightEditActivity.class);
                        startActivity(intent);
                        selectThreeDialog.dismiss();
                    }

                    @Override
                    public void clickTwoButton(SelectThreeDialog selectThreeDialog) {

                    }

                    @Override
                    public void clickThreeButton(SelectThreeDialog selectThreeDialog) {
                        Intent intent = new Intent(MyRegisteredFreightActivity.this,FrequestDeliveryActivity.class);
                        startActivity(intent);
                        selectThreeDialog.dismiss();
                    }

                    @Override
                    public void clickCancelButton() {

                    }
                };
                deliverybefore.getSelectOneButton().setText("수정");
                deliverybefore.getSelectTwoButton().setText("삭제");
                deliverybefore.getSelectThreeButton().setText("화물 배송을 희망한 배송자");
                deliverybefore.show();
                break;
            case 1://배송중
                SelectThreeDialog deliveryafter = new SelectThreeDialog(this) {
                    @Override
                    public void clickOneButton(SelectThreeDialog selectThreeDialog) {
                        Intent intent = new Intent(MyRegisteredFreightActivity.this,DeliveryInfoDetailActivity.class);
                        startActivity(intent);
                        selectThreeDialog.dismiss();
                    }

                    @Override
                    public void clickTwoButton(SelectThreeDialog selectThreeDialog) {
                        Log.d(TAG,"화물관제");
                        Intent intent = new Intent(MyRegisteredFreightActivity.this,FreightManagementActivity.class);
                        startActivity(intent);
                        selectThreeDialog.dismiss();
                    }

                    @Override
                    public void clickThreeButton(SelectThreeDialog selectThreeDialog) {
                        DeliveryAssessDialog deliveryAssessDialog = new DeliveryAssessDialog(getMyRegisteredFreightActivity());
                        deliveryAssessDialog.show();
                        selectThreeDialog.dismiss();
                    }

                    @Override
                    public void clickCancelButton() {

                    }
                };
                deliveryafter.getSelectOneButton().setText("배송자정보");
                deliveryafter.getSelectTwoButton().setText("화물관제");
                deliveryafter.getSelectThreeButton().setText("배송자평가");
                deliveryafter.show();
                break;
            case 2://배송완료
                break;
        }
    }

    private MyRegisteredFreightActivity getMyRegisteredFreightActivity(){
        return this;
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        RecycleUtils.recursiveRecycle(getWindow().getDecorView());
    }
}
