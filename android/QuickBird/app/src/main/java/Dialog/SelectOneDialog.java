package Dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import com.quickbird.quickbird.R;

/**
 * Created by KyoungSik on 2017-03-18.
 * 한가지 선택 다이얼로그
 */
public abstract class SelectOneDialog extends Dialog {

    abstract public void clickOneButton(SelectOneDialog selectOneDialog);
    abstract public void clickCancelButton();

    private Button selectOneButton;//첫번쨰 버튼
    private Button cancelbtn;//취소버튼

    public SelectOneDialog(Context context) {
        super(context);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.setCanceledOnTouchOutside(false);
        setContentView(R.layout.dialog_select_one);
        init();
    }

    public void init(){
        selectOneButton = (Button)findViewById(R.id.dsoselect_btn1);
        selectOneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickOneButton(getSelectOneDialog());
            }
        });

        cancelbtn = (Button)findViewById(R.id.dsocancelbtn);
        cancelbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                clickCancelButton();
            }
        });
    }

    public Button getSelectOneButton() {
        return selectOneButton;
    }


    public SelectOneDialog getSelectOneDialog(){
        return this;
    }
}
