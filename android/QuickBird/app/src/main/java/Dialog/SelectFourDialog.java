package Dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import com.quickbird.quickbird.R;

/**
 * Created by KyoungSik on 2017-03-18.
 * 네개 선택 다이얼로그
 */
public abstract class SelectFourDialog extends Dialog {

    private final String TAG = "SelectFourDialog";

    private Button selectOneButton;//첫번쨰 버튼
    private Button selectTwoButton;//두번쨰 버튼
    private Button selectThreeButton;//세번쨰 버튼
    private Button selectFourButton;//네번째 버튼
    private Button cancelbtn;//취소버튼

    abstract public void clickOneButton(SelectFourDialog selectFourDialog);
    abstract public void clickTwoButton(SelectFourDialog selectFourDialog);
    abstract public void clickThreeButton(SelectFourDialog selectFourDialog);
    abstract public void clickFourButton(SelectFourDialog selectFourDialog);
    abstract public void clickCancelButton();

    public SelectFourDialog(Context context) {
        super(context);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.setCanceledOnTouchOutside(false);
        setContentView(R.layout.dialog_select_four);
        init();
    }

    private void init(){
        //첫번째 버튼
        selectOneButton = (Button)findViewById(R.id.dsfselect_btn1);
        selectOneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickOneButton(getSelectFourDialog());
            }
        });

        //두번째 버튼
        selectTwoButton = (Button)findViewById(R.id.dsfselect_btn2);
        selectTwoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickTwoButton(getSelectFourDialog());
            }
        });

        //세번째 버튼
        selectThreeButton = (Button)findViewById(R.id.dsfselect_btn3);
        selectThreeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickThreeButton(getSelectFourDialog());
            }
        });

        //네번째 버튼
        selectFourButton = (Button)findViewById(R.id.dsfselect_btn4);
        selectFourButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickFourButton(getSelectFourDialog());
            }
        });

        //취소 버튼
        cancelbtn = (Button)findViewById(R.id.dsfcancelbtn);
        cancelbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickCancelButton();
                dismiss();
            }
        });
    }

    public Button getSelectOneButton() {
        return selectOneButton;
    }

    public Button getSelectTwoButton() {
        return selectTwoButton;
    }

    public Button getSelectThreeButton() {
        return selectThreeButton;
    }

    public Button getSelectFourButton() {
        return selectFourButton;
    }

    private SelectFourDialog getSelectFourDialog(){
        return this;
    }
}
