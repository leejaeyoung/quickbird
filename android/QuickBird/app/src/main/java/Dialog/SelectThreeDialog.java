package Dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import com.quickbird.quickbird.R;

/**
 * Created by KyoungSik on 2017-03-17.
 * 세가지 선택 다이얼로그
 */
public abstract class SelectThreeDialog extends Dialog {

    private final String TAG = "SelectThreeDialog";

    private Button selectOneButton;//첫번쨰 버튼
    private Button selectTwoButton;//두번쨰 버튼
    private Button selectThreeButton;//세번쨰 버튼
    private Button cancelbtn;//취소버튼

    abstract public void clickOneButton(SelectThreeDialog selectThreeDialog);
    abstract public void clickTwoButton(SelectThreeDialog selectThreeDialog);
    abstract public void clickThreeButton(SelectThreeDialog selectThreeDialog);
    abstract public void clickCancelButton();


    public SelectThreeDialog(Context context) {
        super(context);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.setCanceledOnTouchOutside(false);
        setContentView(R.layout.dialog_select_three);
        init();
    }

    private void init(){
        //첫번째 버튼
       selectOneButton = (Button)findViewById(R.id.dstselect_btn1);
       selectOneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickOneButton(getSelectThreeDialog());
            }
       });

        //두번쨰 버튼
        selectTwoButton = (Button)findViewById(R.id.dstselect_btn2);
        selectTwoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickTwoButton(getSelectThreeDialog());
            }
        });

        //세번쨰 버튼
        selectThreeButton = (Button)findViewById(R.id.dstselect_btn3);
        selectThreeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickThreeButton(getSelectThreeDialog());
            }
        });

        cancelbtn = (Button)findViewById(R.id.dstcancelbtn);
        cancelbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickCancelButton();
                dismiss();
            }
        });
    }

    public Button getSelectOneButton() {
        return selectOneButton;
    }

    public Button getSelectTwoButton() {
        return selectTwoButton;
    }

    public Button getSelectThreeButton() {
        return selectThreeButton;
    }

    private SelectThreeDialog getSelectThreeDialog(){
        return this;
    }
}
