package Dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Window;

import com.quickbird.quickbird.R;

/**
 * Created by KyoungSik on 2017-03-14.
 * 로딩 다이얼로그
 */
public class Loading extends Dialog {

    private final String TAG = "Loading";

    public Loading(Context context) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE); // 다이얼 로그 제목 삭제
        setContentView(R.layout.dialog_loading); // 다이얼로그 레이아웃
        this.setCanceledOnTouchOutside(false);
        this.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT)); //배경 투명
    }
    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }
}
