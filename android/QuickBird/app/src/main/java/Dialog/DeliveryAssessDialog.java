package Dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;

import com.quickbird.quickbird.R;

/**
 * Created by KyoungSik on 2017-03-20.
 * 배송자 평가하기
 */
public class DeliveryAssessDialog extends Dialog {
    public DeliveryAssessDialog(Context context) {
        super(context);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.setCanceledOnTouchOutside(false);
        setContentView(R.layout.dialog_delivery_assessment);
        init();
    }

    private void init(){
        ImageView exitbtn = (ImageView)findViewById(R.id.ddaexit);
        exitbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }
}
