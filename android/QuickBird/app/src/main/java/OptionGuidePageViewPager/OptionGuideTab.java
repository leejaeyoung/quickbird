package OptionGuidePageViewPager;

import android.app.Activity;
import android.graphics.Color;
import android.view.View;
import android.widget.Button;

import com.quickbird.quickbird.R;

import java.util.ArrayList;

/**
 * Created by KyoungSik on 2017-03-13.
 * 옵션가이드 뷰페이지 탭 버튼
 */
public abstract class OptionGuideTab {

    public final String TAG = "OptionGuideTab";

    public abstract void onClickOptionBtn(int position, OptionGuideTab optionGuideTab);

    public final int option_OnePage = 0;
    public final int option_TwoPage = 1;
    public final int option_ThreePage = 2;

    private Activity act;

    private ArrayList<Button> tabBtns;

    public OptionGuideTab(Activity act){
        this.act = act;
        init();
        //buttonEvent();
    }

    private void init(){
        tabBtns = new ArrayList<Button>();

        Button onebtn = (Button)act.findViewById(R.id.ogpage1btn);
        onebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickOptionBtn(option_OnePage,getOptionGuideTab());
            }
        });
        Button twobtn = (Button)act.findViewById(R.id.ogpage2btn);
        twobtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickOptionBtn(option_TwoPage,getOptionGuideTab());
            }
        });
        Button threebtn = (Button)act.findViewById(R.id.ogpage3btn);
        threebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickOptionBtn(option_ThreePage,getOptionGuideTab());
            }
        });

        tabBtns.add(onebtn);
        tabBtns.add(twobtn);
        tabBtns.add(threebtn);
    }

    /*private void buttonEvent(){
        for(int i=0;i<tabBtns.size();i++){
            tabBtns.get(i).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickOptionBtn(,getOptionGuideTab());
                }
            });
        }
    }*/

    public void setSelectTab(int position){
        for(int i=0;i<tabBtns.size();i++){
            if(position == i) {
                tabBtns.get(position).setBackgroundColor(Color.rgb(255, 116, 59));
            }else{
                tabBtns.get(i).setBackgroundColor(Color.WHITE);
            }
        }
    }

    private OptionGuideTab getOptionGuideTab(){
        return this;
    }
}
