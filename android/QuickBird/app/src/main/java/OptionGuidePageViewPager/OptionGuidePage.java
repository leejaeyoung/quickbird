package OptionGuidePageViewPager;

import android.view.View;
import android.widget.ImageView;

import com.quickbird.quickbird.OptionGuideActivity;
import com.quickbird.quickbird.R;

/**
 * Created by KyoungSik on 2017-03-13.
 * 옵션가이드 뷰페이지의 각 페이지
 */
public class OptionGuidePage {

    private View view;
    private int resource;

    public OptionGuidePage(View view, int resource){
        this.view = view;
        this.resource = resource;
        init();
    }

    private void init(){
        ImageView backgroundimage = (ImageView)view.findViewById(R.id.ogbackgroundimage);
        backgroundimage.setImageResource(resource);
    }
}
