package OptionGuidePageViewPager;

import android.app.Activity;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.quickbird.quickbird.R;

/**
 * Created by KyoungSik on 2017-03-09.
 * 옵션가이드 페이지의 뷰페이저 adapter
 */
public class Og_ViewPageAdapter extends PagerAdapter {

    public final String TAG = "Og_ViewPageAdapter";

    public View[] view;
    private final int viewSize = 3;//뷰페이저의 뷰갯수

    private Activity act;//메인페이지 Activity
    private LayoutInflater inflater;//ViewPage의 각 페이지들을 받아옴

    public final int pageOne = 0;
    public final int pageTwo = 1;
    public final int pageThree = 2;

    int[] imageResource;

    private OptionGuidePage ogOnePage;
    private OptionGuidePage ogTwoPage;
    private OptionGuidePage ogThreePage;

    public Og_ViewPageAdapter(LayoutInflater inflater, Activity act, int[] imageResource){
        this.inflater = inflater;
        this.act = act;
        this.imageResource = imageResource;
        view = new View[viewSize];
    }

    @Override
    public int getCount() {
        return viewSize;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view==object;
    }
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        // TODO Auto-generated method stub
        //ViewPager에서 보이지 않는 View는 제거
        //세번째 파라미터가 View 객체 이지만 데이터 타입이 Object여서 형변환 실시
        container.removeView((View) object);
    }
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        // TODO Auto-generated method stub
        if(view[position]==null) {
            View view1 = null;
            view1 = settingView(view1, position);//페이지별 뷰값 셋팅
            view[position] = view1;
        }
        container.addView(view[position]);
        return view[position];

    }

    private View settingView(View view, int position){
        switch (position){
            case 0:
                view = inflater.inflate(R.layout.og_page,null);
                ogOnePage = new OptionGuidePage(view,imageResource[pageOne]);
                break;
            case 1:
                view = inflater.inflate(R.layout.og_page,null);
                ogTwoPage = new OptionGuidePage(view,imageResource[pageTwo]);
                break;
            case 2:
                view = inflater.inflate(R.layout.og_page,null);
                ogThreePage = new OptionGuidePage(view,imageResource[pageThree]);
                break;
        }
        return view;
    }
}
