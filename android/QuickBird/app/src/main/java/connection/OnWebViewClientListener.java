package connection;

import android.webkit.WebView;

/**
 * Created by KyoungSik on 2017-03-14.
 * 웹뷰 클라이이언트 리스너
 */
public interface OnWebViewClientListener {

    public void startWebClient(WebView webView, String url);
    public void finishWebClient(WebView webView, String url);
    public void errorWebClient(WebView webView, int errorCode, String failingUrl);

}
