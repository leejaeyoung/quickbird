package connection;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

/**
 * Created by KyoungSik on 2017-03-14.
 * 네트워크 체크
 */
public class AppNetWrok {

    // 데이터 사용이 가능한지 체크
    public static Boolean networkCheck(Activity act){
        Boolean check = false;
        ConnectivityManager connmanager = (ConnectivityManager)act.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mobile = connmanager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        NetworkInfo wifi = connmanager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if(mobile.isConnected() || wifi.isConnected()) {
            Log.d("AppNetwork", "success");
            check = true;
        }else {
            Log.d("AppNetwork","fail");
            check = false;
        }
        return check;
    }
}
