package connection;

import android.app.Activity;
import android.graphics.Bitmap;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * Created by KyoungSik on 2017-03-14.
 * 웹 뷰 클래스
 */
public class WebViewClientClass extends WebViewClient {
    private final String Tag = "WebViewClientClass";

    private OnWebViewClientListener onWebViewClientListener;

    private Activity act;

    public WebViewClientClass(Activity act){
        this.act = act;

    }
    @Override
    public void onPageStarted(WebView view, String url, Bitmap favicon) {
        super.onPageStarted(view, url, favicon);
        if(onWebViewClientListener != null)
            onWebViewClientListener.startWebClient(view,url);
    }
    @Override
    public void onPageFinished(WebView view, String url) {
        // 페이지 로딩시 호출된다.
        super.onPageFinished(view, url);
        if(onWebViewClientListener != null)
            onWebViewClientListener.finishWebClient(view,url);
    }
    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        view.loadUrl(url);
        return true;
    }
    @Override
    public void onReceivedError(WebView view, int errorCode,String description, String failingUrl) {
        super.onReceivedError(view, errorCode, description, failingUrl);
        if(onWebViewClientListener != null)
            onWebViewClientListener.errorWebClient(view,errorCode,failingUrl);
        switch(errorCode) {
            case ERROR_AUTHENTICATION:
                Log.d(Tag, "ERROR_AUTHENTICATION");
                break;               // 서버에서 사용자 인증 실패
            case ERROR_BAD_URL:
                Log.d(Tag, "ERROR_BAD_URL");
                break;                           // 잘못된 URL
            case ERROR_CONNECT:
                Log.d(Tag, "ERROR_CONNECT");
                break;                          // 서버로 연결 실패
            case ERROR_FAILED_SSL_HANDSHAKE:
                Log.d(Tag, "ERROR_FAILED_SSL_HANDSHAKE");
                break;    // SSL handshake 수행 실패
            case ERROR_FILE:
                Log.d(Tag, "ERROR_FILE");
                break;                                  // 일반 파일 오류
            case ERROR_FILE_NOT_FOUND:
                Log.d(Tag, "ERROR_FILE_NOT_FOUND");
                break;               // 파일을 찾을 수 없습니다
            case ERROR_HOST_LOOKUP:
                Log.d(Tag, "ERROR_HOST_LOOKUP");
                //   act.setContentView(R.layout.error_activity);
                break;           // 서버 또는 프록시 호스트 이름 조회 실패
            case ERROR_IO:
                Log.d(Tag, "ERROR_IO");
                break;       // 서버에서 읽거나 서버로 쓰기 실패
            case ERROR_PROXY_AUTHENTICATION:
                Log.d(Tag, "ERROR_PROXY_AUTHENTICATION");
                break;   // 프록시에서 사용자 인증 실패
            case ERROR_REDIRECT_LOOP:
                Log.d(Tag, "ERROR_REDIRECT_LOOP");
                break;               // 너무 많은 리디렉션
            case ERROR_TIMEOUT:
                Log.d(Tag, "ERROR_TIMEOUT");
                break;                          // 연결 시간 초과
            case ERROR_TOO_MANY_REQUESTS:
                Log.d(Tag, "ERROR_TOO_MANY_REQUESTS");
                break;     // 페이지 로드중 너무 많은 요청 발생
            case ERROR_UNKNOWN:
                Log.d(Tag, "ERROR_UNKNOWN");
                break;                        // 일반 오류
            case ERROR_UNSUPPORTED_AUTH_SCHEME:
                Log.d(Tag, "ERROR_UNSUPPORTED_AUTH_SCHEME");
                break; // 지원되지 않는 인증 체계
            case ERROR_UNSUPPORTED_SCHEME:
                Log.d(Tag, "ERROR_UNSUPPORTED_SCHEME");
                break;          // URI가 지원되지 않는 방식
        }
    }

    public void setOnWebViewClientListener(OnWebViewClientListener onWebViewClientListener) {
        this.onWebViewClientListener = onWebViewClientListener;
    }
}
