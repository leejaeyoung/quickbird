package WebView;

import android.app.Activity;
import android.webkit.WebSettings;
import android.webkit.WebView;

import connection.WebViewClientClass;

/**
 * Created by KyoungSik on 2017-03-15.
 * 웹뷰 클래스
 */
public class WebViewClass {

    private final String TAG = "WebViewClass";

    private WebView webView;
    private WebViewClientClass webViewClientClass;

    private boolean webLoadFinish = false;

    public WebViewClass(WebView webView, Activity act, String url){
        this.webView = webView;
        init(act, url);
    }

    private void init(Activity act, String url){
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl(url);
        webViewClientClass = new WebViewClientClass(act);
        webView.setWebViewClient(webViewClientClass);
        webView.setVerticalScrollBarEnabled(false);
        webView.clearCache(true);
        webView.getSettings().setAppCacheEnabled(false);
        webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
    }


    public WebViewClientClass getWebViewClientClass() {
        return webViewClientClass;
    }

    public boolean isWebLoadFinish() {
        return webLoadFinish;
    }

    public void setWebLoadFinish(boolean webLoadFinish) {
        this.webLoadFinish = webLoadFinish;
    }

    public WebView getWebView() {
        return webView;
    }
}
