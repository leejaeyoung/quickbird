package Register;

import android.view.View;
import android.widget.EditText;

/**
 * Created by KyoungSik on 2017-03-15.
 * 이미지 아이콘 구조체
 */
public class ImageIcon {
    private View view;
    private EditText editText;
    private int setOffImage;
    private int setOnImage;
    private int imageId;

    public View getView() {
        return view;
    }

    public void setView(View view) {
        this.view = view;
    }

    public EditText getEditText() {
        return editText;
    }

    public void setEditText(EditText editText) {
        this.editText = editText;
    }

    public int getSetOffImage() {
        return setOffImage;
    }

    public void setSetOffImage(int setOffImage) {
        this.setOffImage = setOffImage;
    }

    public int getSetOnImage() {
        return setOnImage;
    }

    public void setSetOnImage(int setOnImage) {
        this.setOnImage = setOnImage;
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }
}
