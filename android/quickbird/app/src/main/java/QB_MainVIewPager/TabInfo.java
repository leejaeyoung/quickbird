package QB_MainVIewPager;

import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class TabInfo {
    private RelativeLayout relativeLayout;
    private TextView badgeText;
    private TextView btnnameText;
    private ImageView btnimageView;

    private String badgeCount="0";

    private int tabonImage;
    private int taboffImage;

    public RelativeLayout getRelativeLayout() {
        return relativeLayout;
    }

    public void setRelativeLayout(RelativeLayout relativeLayout) {
        this.relativeLayout = relativeLayout;
    }

    public ImageView getBtnimageView() {
        return btnimageView;
    }

    public void setBtnimageView(ImageView btnimageView) {
        this.btnimageView = btnimageView;
    }

    public TextView getBtnnameText() {
        return btnnameText;
    }

    public void setBtnnameText(TextView btnnameText) {
        this.btnnameText = btnnameText;
    }

    public int getTabonImage() {
        return tabonImage;
    }

    public void setTabonImage(int tabonImage) {
        this.tabonImage = tabonImage;
    }

    public int getTaboffImage() {
        return taboffImage;
    }

    public void setTaboffImage(int taboffImage) {
        this.taboffImage = taboffImage;
    }

    public TextView getBadgeText() {
        return badgeText;
    }

    public void setBadgeText(TextView badgeText) {
        this.badgeText = badgeText;
    }

    public String getBadgeCount() {
        return badgeCount;
    }

    public void setBadgeCount(String badgeCount) {
        this.badgeCount = badgeCount;
    }
}
