package QB_MainVIewPager.BookMarkPage;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.widget.Button;

import com.quickbird.quickbird.DeliveryInfoDetailActivity;
import com.quickbird.quickbird.FreightInfoDetailActivity;
import com.quickbird.quickbird.R;

import QB_MainVIewPager.HomePage.HomePageWebView;

/**
 * Created by KyoungSik on 2017-03-07.
 * 즐겨찾기 페이지
 */
public class BookMarkPageView {

    private final String TAG = "HomePageView";

    private OnBookMarkPageListener onBookMarkPageListener;

    private Activity act;

    private BookMarkPageWebView bookMarkPageWebView;

    public BookMarkPageView(View view, Activity act){
        this.act = act;
        init(view);
        initWebView(view);
    }

    private void init(View view){
        buttonEvent(view);
    }

    private void buttonEvent(View view){
        Button filterbtn = (Button)view.findViewById(R.id.filterbtn);
        filterbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onBookMarkPageListener != null)
                    onBookMarkPageListener.onClickfilter();
            }
        });
    }

    private void initWebView(View view){

        String url = "http://quickbird.dev.wyhil.com/webview/home.php";

        WebView webView = (WebView)view.findViewById(R.id.bookmarkwebview);
        bookMarkPageWebView = new BookMarkPageWebView(webView,act,url);
        initWebViewJavascript();
    }

    private void initWebViewJavascript(){
        bookMarkPageWebView.getWebView().addJavascriptInterface(new Object(){

            @JavascriptInterface
            public void h_list(int state){
                Log.d(TAG, "h_list");
                if(state == 0){//화물
                    Intent intent = new Intent(act, FreightInfoDetailActivity.class);
                    act.startActivity(intent);
                }else if(state == 1){//배송자
                    Intent intent = new Intent(act, DeliveryInfoDetailActivity.class);
                    act.startActivity(intent);
                }
            }
        },"quickbird");

    }

    public BookMarkPageWebView getBookMarkPageWebView() {
        return bookMarkPageWebView;
    }

    public void setOnBookMarkPageListener(OnBookMarkPageListener onBookMarkPageListener) {
        this.onBookMarkPageListener = onBookMarkPageListener;
    }
}
