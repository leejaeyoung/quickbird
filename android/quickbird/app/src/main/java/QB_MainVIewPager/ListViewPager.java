package QB_MainVIewPager;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;

/**
 * Created by KyoungSik on 2017-03-02
 * 뷰페이지 스크롤 컨트롤
 */
public class ListViewPager extends ViewPager {
    private final String TAG = "ListViewPager";
    private float viewX = 0;
    private float viewY = 0;
    public ListViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
    //가로 세로 스크롤 부드럽게하기
    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
       // Log.d(TAG, "onInterceptTouchEvent");
        float getX = ev.getX();
        float getY = ev.getY();
        if(ev.getAction() == MotionEvent.ACTION_DOWN){
            viewX = ev.getX();
            viewY = ev.getY();
        }
        //Log.d(Tag, "Math.abs(getViewX()-getX) : " + Math.abs(getViewX() - getX) + " Math.abs(getViewY()-getY) : " + Math.abs(getViewY() - getY));
        if(Math.abs(getViewX() - getX) < Math.abs(getViewY() - getY)){
            return false;
        }else{
            return super.onInterceptTouchEvent(ev);
        }
        //return super.onInterceptTouchEvent(ev);
    }
    public float getViewX(){
        return viewX;
    }

    public float getViewY(){
        return viewY;
    }
    public void setViewX(float viewX){
        this.viewX = viewX;
    }
    public void setViewY(float viewY){
        this.viewY = viewY;
    }
}
