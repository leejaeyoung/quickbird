package QB_MainVIewPager;

import android.app.Activity;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.quickbird.quickbird.R;

import QB_MainVIewPager.BookMarkPage.BookMarkPageView;
import QB_MainVIewPager.HomePage.HomePageView;
import QB_MainVIewPager.MyPage.MyPageView;
import QB_MainVIewPager.OptionPage.OptionPageView;
import QB_MainVIewPager.SurroundingDeliveryPage.SurroundingDeliveryPageView;

/**
 * Created by KyoungSik on 2017-03-03.
 * 메인 뷰페이지의  adapter
 */
public class QB_ViewPageAdapter extends PagerAdapter{

    public final String TAG = "QB_ViewPageAdapter";

    private final int viewSize = 5;
    public View[] view;

    private Activity act;//메인페이지 Activity
    private LayoutInflater inflater;//ViewPage의 각 페이지들을 받아옴

    private HomePageView homePageView;// 홈 페이지
    private BookMarkPageView bookMarkPageView;// 즐겨찾기 페이지
    private MyPageView myPageView;//마이페이지
    private SurroundingDeliveryPageView surroundingDeliveryPageView;//주변배송자 페이지
    private OptionPageView optionPageView;//설정 페이지

    public final int homePage = 0;
    public final int bookmarkPage = 1;
    public final int surroundingDeliveryPage = 2;
    public final int myPage = 3;
    public final int optionPage = 4;

    public QB_ViewPageAdapter(LayoutInflater inflater, Activity act){
        this.inflater = inflater;
        this.act = act;
        view = new View[viewSize];
    }

    @Override
    public int getCount() {
        return viewSize;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view==object;
    }
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        // TODO Auto-generated method stub
        //ViewPager에서 보이지 않는 View는 제거
        //세번째 파라미터가 View 객체 이지만 데이터 타입이 Object여서 형변환 실시
        container.removeView((View) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        // TODO Auto-generated method stub
        if(view[position]==null) {
            View view1 = null;
            view1 = settingView(view1, position);//페이지별 뷰값 셋팅
            view[position] = view1;
        }
        container.addView(view[position]);
        return view[position];

    }

    private View settingView(View view, int position){
        switch (position){
            case 0:
                view = inflater.inflate(R.layout.qb_main_homepage,null);
                homePageView = new HomePageView(view,act);
                break;
            case 1:
                view = inflater.inflate(R.layout.qb_main_bookmarkpage,null);
                bookMarkPageView = new BookMarkPageView(view,act);
                break;
            case 2:
                view = inflater.inflate(R.layout.qb_main_surrounddelivery,null);
                surroundingDeliveryPageView = new SurroundingDeliveryPageView(view,act);
                break;
            case 3:
                view = inflater.inflate(R.layout.qb_main_mypage,null);
                myPageView = new MyPageView(view,act);
                break;
            case 4:
                view = inflater.inflate(R.layout.qb_main_optionpage,null);
                optionPageView = new OptionPageView(view,act);
                break;
        }
        return view;
    }

    public int pageState(int position){
        if(position == 0){
            return homePage;
        }else if(position == 1){
            return bookmarkPage;
        }else if(position == 2){
            return surroundingDeliveryPage;
        }else if(position == 3){
            return myPage;
        }else if(position == 4){
            return optionPage;
        }
        return -1;
    }

    public HomePageView getHomePageView() {
        return homePageView;
    }

    public BookMarkPageView getBookMarkPageView() {
        return bookMarkPageView;
    }

    public MyPageView getMyPageView() {
        return myPageView;
    }

    public SurroundingDeliveryPageView getSurroundingDeliveryPageView() {
        return surroundingDeliveryPageView;
    }

    public OptionPageView getOptionPageView() {
        return optionPageView;
    }
}
