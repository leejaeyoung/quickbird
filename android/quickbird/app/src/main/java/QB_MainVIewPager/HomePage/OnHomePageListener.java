package QB_MainVIewPager.HomePage;

/**
 * Created by KyoungSik on 2017-03-06.
 */
public interface OnHomePageListener {
    public void onClickSearch();
    public void onClickWrite();
}
