package QB_MainVIewPager.HomePage;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.LinearLayout;

import com.quickbird.quickbird.CompanyUserRegisterActivity;
import com.quickbird.quickbird.DeliveryInfoDetailActivity;
import com.quickbird.quickbird.FreightInfoDetailActivity;
import com.quickbird.quickbird.PrivateUserRegisterActivity;
import com.quickbird.quickbird.R;

import Dialog.SelectTwoDialog;

/**
 * Created by KyoungSik on 2017-03-03.
 *  홈 페이지
 */
public class HomePageView {

    private final String TAG = "HomePageView";

    private OnHomePageListener onHomePageListener;

    private Activity act;
    private HomePageWebView homePageWebView;

    public HomePageView(View view, Activity act){
        this.act = act;
        init(view);
        initWebView(view);

    }

    private void init(View view){
        buttonEvent(view);
    }

    private void buttonEvent(View view){
        //검색 필터 버튼
        Button searchbtn = (Button)view.findViewById(R.id.searchbtn);
        searchbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "searchbtn click");
                //onOpenLeftDrawer();
                if(onHomePageListener != null)
                    onHomePageListener.onClickSearch();
            }
        });

        //등록 버튼
        Button writebtn = (Button) view.findViewById(R.id.writebtn);
        writebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onHomePageListener != null)
                    onHomePageListener.onClickWrite();
            }
        });

    }

    private void initWebView(View view){

        String url = "http://quickbird.dev.wyhil.com/webview/home.php";

        WebView webView = (WebView)view.findViewById(R.id.homepagewebview);
        homePageWebView = new HomePageWebView(webView,act,url);
        initWebViewJavascript();

    }

    private void initWebViewJavascript(){
        homePageWebView.getWebView().addJavascriptInterface(new Object(){
            @JavascriptInterface
            public void h_list(int state){
                Log.d(TAG, "h_list");
                if(state == 0){//화물
                    Intent intent = new Intent(act, FreightInfoDetailActivity.class);
                    act.startActivity(intent);
                }else if(state == 1){//배송자
                    Intent intent = new Intent(act, DeliveryInfoDetailActivity.class);
                    act.startActivity(intent);
                }
            }
        },"quickbird");
    }

    public HomePageWebView getHomePageWebView() {
        return homePageWebView;
    }

    /* 홈 페이지뷰 이벤트 리스너 설정
        *
        * */
    public void setOnHomePageListener(OnHomePageListener onHomePageListener){
        this.onHomePageListener = onHomePageListener;
    }
}
