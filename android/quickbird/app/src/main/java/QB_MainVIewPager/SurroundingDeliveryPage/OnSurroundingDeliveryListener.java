package QB_MainVIewPager.SurroundingDeliveryPage;

/**
 * Created by KyoungSik on 2017-03-08.
 * 주변 배송자 이벤트 리스너
 */
public interface OnSurroundingDeliveryListener {
    public void onClickSearch(SurroundingDeliveryPageView surroundingDeliveryPageView);
    public void onClickReturn(SurroundingDeliveryPageView surroundingDeliveryPageView);
}
