package QB_MainVIewPager.SurroundingDeliveryPage;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.widget.ActionMenuView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.quickbird.quickbird.DeliveryInfoDetailActivity;
import com.quickbird.quickbird.FreightInfoDetailActivity;
import com.quickbird.quickbird.R;

import QB_MainVIewPager.HomePage.HomePageWebView;

/**
 * Created by KyoungSik on 2017-03-08.
 * 주변 배송자 페이지
 */
public class SurroundingDeliveryPageView {

    private final String TAG = "SurroundingDelivery";

    private OnSurroundingDeliveryListener onSurroundingDeliveryListener;

    private SurroundingDeliveryWebView surroundingDeliveryWebView;
    private Activity act;
    private View view;
    private LinearLayout delivery_plinear;//배송자 정보
    private boolean dpState = false;

    public SurroundingDeliveryPageView(View view, Activity act){
        this.act = act;
        this.view = view;
        init(view);
        initWebView(view);
        webPageEvent();
    }

    private void init(View view){
        delivery_pinit(view);
        buttonEvent(view);

    }

    private void delivery_pinit(View view){
        delivery_plinear = (LinearLayout)view.findViewById(R.id.delivery_plinear);

    }

    /* 버튼 이벤트
    *
    * */
    private void buttonEvent(View view){
        //검색 버튼
        Button searchbtn = (Button)view.findViewById(R.id.sudesearchbtn);
        searchbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            //    if(!dpState){

             //   }
                if(onSurroundingDeliveryListener != null)
                    onSurroundingDeliveryListener.onClickSearch(getSurroundigDeliveryPageView());
            }
        });

        //갱신 버튼
        Button returnbtn = (Button)view.findViewById(R.id.sudereturnbtn);
        returnbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onSurroundingDeliveryListener != null)
                    onSurroundingDeliveryListener.onClickReturn(getSurroundigDeliveryPageView());
            }
        });

        ImageView delivery_pexit = (ImageView)view.findViewById(R.id.deliverypexit);
        delivery_pexit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "delivery_pexit");

                downAniMove(delivery_plinear);
                dpState = false;
            }
        });

        Button deliveryCall = (Button)view.findViewById(R.id.deliverycall);
        deliveryCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG,"deliveryCall");
            }
        });
    }

    private void webPageEvent(){
        surroundingDeliveryWebView.getWebView().addJavascriptInterface(new Object(){
            @JavascriptInterface
            public void marker_click(int seq){
                Log.d(TAG, "marker_click");
                upAniMove(delivery_plinear);
                dpState = true;

            }
        },"quickbird");
    }

    /* 주변 배송자 위치 알려주는 웹뷰
    *
    * */
    private void initWebView(View view){
        String url = "http://quickbird.dev.wyhil.com/webview/naver_map.php";

        WebView webView = (WebView)view.findViewById(R.id.surrounddeliverywebview);
        surroundingDeliveryWebView = new SurroundingDeliveryWebView(webView,act,url);
    }

    public void upAniMove(LinearLayout linearLayout){
        TranslateAnimation move = new TranslateAnimation
                (0,   // fromXDelta
                        0,  // toXDelta
                        linearLayout.getHeight(),    // fromYDelta
                        0);// toYDelta
        move.setDuration(200);
        move.setFillAfter(true);
        move.setFillEnabled(true);
        linearLayout.startAnimation(move);
        move.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                delivery_plinear.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });


    }
    public void downAniMove(final LinearLayout linearLayout){
        TranslateAnimation move = new TranslateAnimation
                (0,   // fromXDelta
                        0,  // toXDelta
                        0,    // fromYDelta
                        linearLayout.getHeight());// toYDelta
        move.setDuration(200);
        move.setFillAfter(true);
        move.setFillEnabled(true);
        linearLayout.startAnimation(move);
        move.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                delivery_plinear.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    public void setOnSurroundingDeliveryListener(OnSurroundingDeliveryListener onSurroundingDeliveryListener) {
        this.onSurroundingDeliveryListener = onSurroundingDeliveryListener;
    }

    public SurroundingDeliveryPageView getSurroundigDeliveryPageView(){
        return this;
    }

    public SurroundingDeliveryWebView getSurroundingDeliveryWebView() {
        return surroundingDeliveryWebView;
    }



}
