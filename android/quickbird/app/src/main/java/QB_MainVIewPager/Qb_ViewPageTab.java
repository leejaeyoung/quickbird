package QB_MainVIewPager;

import android.app.Activity;
import android.graphics.Color;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.quickbird.quickbird.R;

/**
 * Created by KyoungSik on 2017-03-03.
 * 메인 뷰페이지 탭
 */
public abstract class Qb_ViewPageTab {
    public final String TAG = "Qb_ViewPageTab";

    private final int maxArrayLength = 5;//푸터 갯수
    /*
    * 푸터 상태 값
    * */
    public final int under_home = 0;//홈 버튼
    public final int under_bookmark = 1;//즐겨찾기 버튼
    public final int under_su_delivery = 2;//주변배송자 버튼
    public final int under_mypage = 3;//마이페이지 버튼
    public final int under_option = 4;//설정 버튼

    private TabInfo[] tabInfos;//각 버튼 값들을 담은 배열

    /* 각 버튼을 클릭 했을 시 실행 함수
    * underbtn : 터치한 버튼 값
    * qb_viewpageTab
    * */
    public abstract void touchListenear(int underbtn, Qb_ViewPageTab qb_viewpageTab);//푸터 각 이미지 터치했을시 실행 함수

    public Qb_ViewPageTab(Activity act){
        init(act);
    }

    private void init(Activity act){

        RelativeLayout rel_home = (RelativeLayout) act.findViewById(R.id.rel_home);

        RelativeLayout rel_bookmark = (RelativeLayout) act.findViewById(R.id.rel_bookmark);

        RelativeLayout rel_su_delivery = (RelativeLayout) act.findViewById(R.id.rel_su_delivery);

        RelativeLayout rel_mypage = (RelativeLayout) act.findViewById(R.id.rel_mypage);

        RelativeLayout rel_option = (RelativeLayout) act.findViewById(R.id.rel_option);

        tabInfos = new TabInfo[maxArrayLength];
        for(int i=0;i<maxArrayLength;i++){
            tabInfos[i] = new TabInfo();
        }


        //홈 버튼 정보 저장
        tabInfos[0].setRelativeLayout(rel_home);
        TextView homeText = (TextView)act.findViewById(R.id.homebadgeText);
        TextView homebntText = (TextView)act.findViewById(R.id.homebtnText);
        ImageView homeImage = (ImageView)act.findViewById(R.id.qb_home_imageView);
        tabInfos[0].setBtnnameText(homebntText);
        tabInfos[0].setBadgeText(homeText);
        tabInfos[0].setTabonImage(R.drawable.icon_footer_home_on);
        tabInfos[0].setTaboffImage(R.drawable.icon_footer_home_off);
        tabInfos[0].setBtnimageView(homeImage);
        setBadgeView(under_home, "0");

        //즐겨찾기 버튼 정보 저장
        tabInfos[1].setRelativeLayout(rel_bookmark);
        TextView bookmarkText = (TextView)act.findViewById(R.id.bookmarkbadgeText);
        TextView bookbntText = (TextView)act.findViewById(R.id.bookmarkbtnText);
        ImageView bookmarkImage = (ImageView)act.findViewById(R.id.qb_bookmark_imageView);
        tabInfos[1].setBtnnameText(bookbntText);
        tabInfos[1].setBadgeText(bookmarkText);
        tabInfos[1].setTabonImage(R.drawable.icon_footer_box_on);
        tabInfos[1].setTaboffImage(R.drawable.icon_footer_box_off);
        tabInfos[1].setBtnimageView(bookmarkImage);
        setBadgeView(under_bookmark, "0");

        //주변배송자 버튼 정보 저장
        tabInfos[2].setRelativeLayout(rel_su_delivery);
        TextView sudeliveryText = (TextView)act.findViewById(R.id.sudeliverybadgeText);
        TextView sudeliverybtnText = (TextView)act.findViewById(R.id.sudeliverybtnText);
        ImageView sudeliveryImage = (ImageView)act.findViewById(R.id.qb_su_delivery_imageView);
        tabInfos[2].setBtnnameText(sudeliverybtnText);
        tabInfos[2].setBadgeText(sudeliveryText);
        tabInfos[2].setTabonImage(R.drawable.icon_footer_map_on);
        tabInfos[2].setTaboffImage(R.drawable.icon_footer_map_off);
        tabInfos[2].setBtnimageView(sudeliveryImage);
        setBadgeView(under_su_delivery, "0");

        //마이페이지 버튼 정보 저장
        tabInfos[3].setRelativeLayout(rel_mypage);
        TextView mypageText = (TextView)act.findViewById(R.id.mypagebadgeText);
        TextView mypagebtnText = (TextView)act.findViewById(R.id.mypagebtnText);
        ImageView mypageImage = (ImageView)act.findViewById(R.id.qb_mypage_imageView);
        tabInfos[3].setBtnnameText(mypagebtnText);
        tabInfos[3].setBadgeText(mypageText);
        tabInfos[3].setTabonImage(R.drawable.icon_footer_mypage_on);
        tabInfos[3].setTaboffImage(R.drawable.icon_footer_mypage_off);
        tabInfos[3].setBtnimageView(mypageImage);
        setBadgeView(under_mypage, "0");

        //설정 버튼 정보 저장
        tabInfos[4].setRelativeLayout(rel_option);
        TextView optionText = (TextView)act.findViewById(R.id.optionbadgeText);
        TextView optionbtnText = (TextView)act.findViewById(R.id.optionbtnText);
        ImageView optionImage = (ImageView)act.findViewById(R.id.qb_option_imageView);
        tabInfos[4].setBtnnameText(optionbtnText);
        tabInfos[4].setBadgeText(optionText);
        tabInfos[4].setTabonImage(R.drawable.icon_footer_admin_on);
        tabInfos[4].setTaboffImage(R.drawable.icon_footer_admin_off);
        tabInfos[4].setBtnimageView(optionImage);
        setBadgeView(under_option, "0");


        //푸터 터치 이벤트
        for (int i = 0; i < maxArrayLength; i++) {
            tabInfos[i].getRelativeLayout().setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {

                    if (v.getId() == R.id.rel_home) {
                        touchListenear(under_home, getQb_viewPageTab());
                        Log.d(TAG, "rel_home");
                    } else if (v.getId() == R.id.rel_bookmark) {
                        touchListenear(under_bookmark, getQb_viewPageTab());
                        Log.d(TAG, "rel_bookmark");
                    } else if (v.getId() == R.id.rel_su_delivery) {
                        touchListenear(under_su_delivery, getQb_viewPageTab());
                        Log.d(TAG, "rel_su_delivery");
                    } else if (v.getId() == R.id.rel_mypage) {
                        touchListenear(under_mypage, getQb_viewPageTab());
                        Log.d(TAG, "rel_mypage");
                    } else if (v.getId() == R.id.rel_option) {
                        touchListenear(under_option, getQb_viewPageTab());
                        Log.d(TAG, "rel_option");
                    }
                    return false;
                }
            });
        }
    }

    public void setBtnImage(int position){
        for(int i=0; i<maxArrayLength; i++){
            if(i==position){
                tabInfos[i].getBtnimageView().setBackgroundResource(tabInfos[i].getTabonImage());
                tabInfos[i].getBtnnameText().setTextColor(Color.rgb(254, 159, 101));
            }else{
                tabInfos[i].getBtnimageView().setBackgroundResource(tabInfos[i].getTaboffImage());
                tabInfos[i].getBtnnameText().setTextColor(Color.rgb(185, 195, 194));
            }
        }
    }

    /*
푸터 오프 이미지 셋팅
* */
    public void setOffImage() {
        for(int i=0; i<maxArrayLength; i++){
            tabInfos[i].getRelativeLayout().setBackgroundColor(Color.WHITE);
        }
    }

    /* 선택한 푸터 이미지 온
       state : 클릭한 버튼
  * */
    public void setOnImage(int state) {
        for(int i=0; i<maxArrayLength; i++){
            if(state == i) {
                tabInfos[i].getRelativeLayout().setBackgroundColor(Color.GRAY);
            }
        }
    }

    public void setBadgeView(int state,String badgeCount){
        if(badgeCount!=null) {
            tabInfos[state].setBadgeCount(badgeCount);
            if (badgeCount.matches("0")) {
                tabInfos[state].getBadgeText().setVisibility(View.INVISIBLE);
            } else {
                tabInfos[state].getBadgeText().setVisibility(View.VISIBLE);
                tabInfos[state].getBadgeText().setText(badgeCount);
            }
        }
    }

    public int getBadgeCoount(int state){
        Log.d(TAG,"state = "+tabInfos[state].getBadgeCount());
        return Integer.parseInt(tabInfos[state].getBadgeCount());
    }

    public Qb_ViewPageTab getQb_viewPageTab(){
        return this;
    }
}
