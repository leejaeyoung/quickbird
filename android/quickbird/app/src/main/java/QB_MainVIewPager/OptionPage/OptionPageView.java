package QB_MainVIewPager.OptionPage;

import android.app.Activity;
import android.app.VoiceInteractor;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.quickbird.quickbird.R;

import SearchLayout.SearchLayout;
import SearchLayout.OnSearchLayoutListener;
/**
 * Created by KyoungSik on 2017-03-13.
 * 메인 뷰페이저의 설정 페이지
 *
 */
public class OptionPageView {

    private final String TAG = "OptionPageView";

    private View view;
    private Activity act;

    private boolean alarmState = false;//알람설정 버튼 상태

    public OptionPageView(View view, Activity act){
        this.act = act;
        init(view);
    }

    private void init(View view){
        LinearLayout search_linear = (LinearLayout)view.findViewById(R.id.optionlienar);
        OptionSearchLayout filterLayout = new OptionSearchLayout(search_linear,act);
        filterLayout.setOnOptionSearchLayoutListener(new OnOptionSearchLayoutListener() {
            @Override
            public void onClickSearchState(int state) {
                Log.d(TAG, "onClickSearchState : " + state);
            }

            @Override
            public void onClickItem(int position, String[] titlelist) {
                Log.d(TAG, "titllist : " + titlelist[position]);
            }
        });

        //알람설정 버튼
        Button alarmbtn = (Button)view.findViewById(R.id.alarmbtn);
        alarmbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!alarmState){
                    v.setBackgroundResource(R.drawable.icon_alarm_on);
                    alarmState = true;
                }else{
                    v.setBackgroundResource(R.drawable.icon_alarm_off);
                    alarmState = false;
                }
            }
        });
    }

    public OptionPageView getOptionPageView(){
        return this;
    }
}
