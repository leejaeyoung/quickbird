package QB_MainVIewPager.MyPage;

import android.view.View;

/**
 * Created by KyoungSik on 2017-03-07
 * 마이페이지 정보 이벤트 리스너.
 */
public interface OnMyListInfoListener {
    /*public void onClickListUp(View v,int state);
    public void onClickListDown(View v,int state);*/

    public void onClickListItem(View v, int state, MyPageListView myPageListView);
}
