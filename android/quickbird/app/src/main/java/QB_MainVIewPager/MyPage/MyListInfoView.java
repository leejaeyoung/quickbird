package QB_MainVIewPager.MyPage;

import android.graphics.Color;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;

import com.quickbird.quickbird.R;

/**
 * Created by KyoungSik on 2017-03-07.
 * 마이페이지 리스트 정보
 */
public class MyListInfoView {

    private final String TAG = "MyListInfoView";

    private final int linearMAX = 3;

    public final int USER_INFO_EDIT = 0;
    public final int MY_REGISTERED_FREIGHT = 1;
    public final int MY_REGISTERED_DELIVERY = 2;

    private OnMyListInfoListener onMyListInfoListener;

    private LinearLayout linearTab[];

    private boolean touchState = false;//더블 터치 막기

    private int currentEvent = 0;//현재 클릭된 리스트

    public MyListInfoView(View view){
       // init(view);
    }

   /* public void init(View view){
        linearTab = new LinearLayout[linearMAX];
        LinearLayout li_useredit = (LinearLayout)view.findViewById(R.id.usereditlinear);
        li_useredit.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == event.ACTION_DOWN){
                    Log.d(TAG, "ACTION_DOWN");
                    if(!touchState){
                        touchState = true;
                    }
                    if(touchState) {//터치된상태
                        currentEvent = USER_INFO_EDIT;
                        if (onMyListInfoListener != null)
                            onMyListInfoListener.onClickListDown(v, USER_INFO_EDIT);
                    }
                }else if(event.getAction() == event.ACTION_UP){
                    Log.d(TAG, "ACTION_UP");
                    if(currentEvent == USER_INFO_EDIT) {//현재 클릭된 이벤트상태가 회원정보 수정 상태
                        touchState = false;
                        if (onMyListInfoListener != null)
                            onMyListInfoListener.onClickListUp(v, USER_INFO_EDIT);
                    }else{
                        v.setBackgroundColor(Color.WHITE);
                    }
                }
                return true;
            }
        });
        linearTab[0] = li_useredit;

        LinearLayout li_myrf = (LinearLayout)view.findViewById(R.id.myrfreightlinear);
        li_myrf.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == event.ACTION_DOWN){
                    Log.d(TAG, "ACTION_DOWN");
                    if(!touchState){
                        touchState = true;
                    }
                    if(touchState) {//터치된상태
                        currentEvent = MY_REGISTERED_FREIGHT;
                        if (onMyListInfoListener != null)
                            onMyListInfoListener.onClickListDown(v, MY_REGISTERED_FREIGHT);
                    }
                }else if(event.getAction() == event.ACTION_UP){
                    Log.d(TAG, "ACTION_UP");
                    if(currentEvent == MY_REGISTERED_FREIGHT) {//현재 클릭된 이벤트상태가 내가 등록한 화물
                        touchState = false;
                        if (onMyListInfoListener != null)
                            onMyListInfoListener.onClickListUp(v, MY_REGISTERED_FREIGHT);
                    }else{
                        v.setBackgroundColor(Color.WHITE);
                    }
                }
                return true;
            }
        });
        linearTab[1] = li_myrf;

        LinearLayout li_myrd = (LinearLayout)view.findViewById(R.id.myrdeliverylinear);
        li_myrd.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == event.ACTION_DOWN){
                    Log.d(TAG, "ACTION_DOWN");
                    if(!touchState){
                        touchState = true;
                    }
                    if(touchState) {//터치된상태
                        currentEvent = MY_REGISTERED_DELIVERY;
                        if (onMyListInfoListener != null)
                            onMyListInfoListener.onClickListDown(v, MY_REGISTERED_DELIVERY);
                    }
                }else if(event.getAction() == event.ACTION_UP){
                    Log.d(TAG, "ACTION_UP");
                    if(currentEvent == MY_REGISTERED_DELIVERY) {//현재 클릭된 이벤트상태가 내가 등록한 배송자
                        touchState = false;
                        if (onMyListInfoListener != null)
                            onMyListInfoListener.onClickListUp(v, MY_REGISTERED_DELIVERY);
                    }else{
                        v.setBackgroundColor(Color.WHITE);
                    }
                }
                return true;
            }
        });
        linearTab[2] = li_myrd;
    }*/

    public void setChangedinit(){
        for(int i=0;i<linearMAX;i++) {
            linearTab[i].setBackgroundColor(Color.WHITE);
        }
        touchState = false;
    }

    public void setOnMyListInfoListener(OnMyListInfoListener onMyListInfoListener) {
        this.onMyListInfoListener = onMyListInfoListener;
    }

    public LinearLayout[] getLinearTab() {
        return linearTab;
    }

    public int getCurrentEvent() {
        return currentEvent;
    }
}
