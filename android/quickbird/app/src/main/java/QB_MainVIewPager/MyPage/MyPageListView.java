package QB_MainVIewPager.MyPage;

import android.app.Activity;
import android.graphics.Color;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.quickbird.quickbird.R;

import SearchLayout.OnSearchLayoutListener;

/**
 * Created by KyoungSik on 2017-03-07.
 * 마이페이지 리스트 뷰
 */
public class MyPageListView {

    private final String TAG = "MyPageListView";

    public final int USER_INFO_EDIT = 0;
    public final int MY_REGISTERED_FREIGHT = 1;
    public final int MY_REGISTERED_DELIVERY = 2;

    private ListView listView;

    private View beforeView;//이전 선택한 리스트 뷰 아이템

    private OnMyListInfoListener onMyListInfoListener;//리스트뷰 이벤트 리스너


    public MyPageListView(ListView listView, Activity act){
       this.listView = listView;
        initAdapter(act);

    }

    private void initAdapter(Activity act){
        String[] arrayList = act.getResources().getStringArray(R.array.mypage_list);
        MyPageListAdapter myPageListAdapter = new MyPageListAdapter(act,R.layout.mypage_listview_cell,arrayList);
        listView.setAdapter(myPageListAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(beforeView != null){
                    beforeView.setBackgroundColor(Color.WHITE);
                }
                beforeView = view;
                if(onMyListInfoListener != null){
                    onMyListInfoListener.onClickListItem(view, position, getMyPageListView());
                }
            }
        });
    }

    private MyPageListView getMyPageListView(){
        return this;
    }

    public void setOnMyListInfoListener(OnMyListInfoListener onMyListInfoListener) {
        this.onMyListInfoListener = onMyListInfoListener;
    }
}
