package QB_MainVIewPager.MyPage;

import android.app.Activity;
import android.view.View;
import android.widget.ListView;

import com.quickbird.quickbird.R;

/**
 * Created by KyoungSik on 2017-03-07.
 * 마이페이지
 */
public class MyPageView {

    private final String TAG = "MyPageView";

    private View view;
    private Activity act;

   // private MyListInfoView myListInfoView;

    private MyPageListView myPageListView;

    public MyPageView(View view, Activity act){
        this.view = view;
        this.act = act;
      //  myListInfoView = new MyListInfoView(view);
        initListView();
    }

    public void initListView(){
        ListView listView = (ListView)view.findViewById(R.id.listView);
        myPageListView = new MyPageListView(listView,act);
    }

    public MyPageListView getMyPageListView() {
        return myPageListView;
    }

    /* public MyListInfoView getMyListInfoView() {
        return myListInfoView;
    }*/
}
