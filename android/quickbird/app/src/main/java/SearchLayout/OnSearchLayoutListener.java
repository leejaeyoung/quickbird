package SearchLayout;

/**
 * Created by KyoungSik on 2017-03-12.
 * 검색 필터
 */
public interface OnSearchLayoutListener {
    public void onClickSearchState(int state);
    public void onClickItem(int position, String[] titlelist);
    public void onClickSearchBtn();
    public void onClickCancelBtn();

}
