package SearchLayout;

import android.app.Activity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.quickbird.quickbird.R;

import java.util.ArrayList;

/**
 * Created by KyoungSik on 2017-03-13.
 * 검색 레이아웃 검색
 */
public abstract class SearchState {

    private final String TAG = "SearchState";

    public abstract void onClickButton(int state,SearchState searchState);

    private final int MAX_SIZE = 3;

    public final int SEARCH_ALL = 0;//전체
    public final int SEARCH_FREIGHT = 1;//화물
    public final int SEARCH_DELIVERY = 2;//배송자

    private boolean searchallState = false;//전체 체크박스 상태
    private boolean searchfreightState = false;//화물 체크박스 상태
    private boolean searchdeliveryState = false;//배송자 체크박스 상태

    private ArrayList<Button> searchbtn;

    private Activity act;

    public SearchState(LinearLayout linearLayout, Activity act){
        this.act = act;
        init(linearLayout);
        initButtonEvent();
    }


    private void init(LinearLayout linearLayout){
        searchbtn = new ArrayList<Button>();

        Button searchallbtn = (Button)linearLayout.findViewById(R.id.searchallbtn);
        Button searchFreight = (Button)linearLayout.findViewById(R.id.searchfreightbtn);
        Button searchDelivery = (Button)linearLayout.findViewById(R.id.searchdeliverybtn);

        searchbtn.add(searchallbtn);
        searchbtn.add(searchFreight);
        searchbtn.add(searchDelivery);
    }

    /* 버튼 이벤트
    *
    * */
    private void initButtonEvent(){
        searchbtn.get(SEARCH_ALL).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!searchallState){
                    searchallState = true;
                    v.setBackgroundResource(R.drawable.icon_check_on);
                }else{
                    searchallState = false;
                    v.setBackgroundResource(R.drawable.icon_check_off);
                }
                onClickButton(SEARCH_ALL, getSearchState());
            }
        });

        searchbtn.get(SEARCH_FREIGHT).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!searchfreightState){
                    searchfreightState = true;
                    v.setBackgroundResource(R.drawable.icon_check_on);
                }else{
                    searchfreightState = false;
                    v.setBackgroundResource(R.drawable.icon_check_off);
                }
                onClickButton(SEARCH_FREIGHT, getSearchState());
            }
        });

        searchbtn.get(SEARCH_DELIVERY).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!searchdeliveryState){
                    searchdeliveryState = true;
                    v.setBackgroundResource(R.drawable.icon_check_on);
                }else{
                    searchdeliveryState = false;
                    v.setBackgroundResource(R.drawable.icon_check_off);
                }
                onClickButton(SEARCH_DELIVERY, getSearchState());
            }
        });
    }

    private SearchState getSearchState(){
        return this;
    }

}
