package Dialog;

import android.app.Dialog;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.quickbird.quickbird.R;

/**
 * Created by KyoungSik on 2017-03-13.
 * 비밀번호 찾기 다이얼로그
 */
public abstract class FindPwDialog extends Dialog {

    private final String TAG = "FindPwDialog";



    public abstract void onClickConfirm(String emailid, FindPwDialog findPwDialog);
    public abstract void onClickCancel();

    private EditText emailidText;
    private ImageView emailidicon;

    public FindPwDialog(Context context) {
        super(context);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.setCanceledOnTouchOutside(false);
        setContentView(R.layout.dialog_findpw);
        init();
    }

    private void init(){
        emailidText = (EditText)findViewById(R.id.dfemailidText);
        emailidicon = (ImageView)findViewById(R.id.dfemaliidimage);
        buttonEvent();
        editTextEvent();
    }

    /* EditText 포커스 바꼈을시 아이콘 이미지 상태 바꿈
    *
    * */
    private void editTextEvent(){
        emailidText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    emailidicon.setImageResource(R.drawable.icon_email_on);
                } else {
                    emailidicon.setImageResource(R.drawable.icon_email_off);
                }
            }
        });
    }

    //버튼이벤트
    private void buttonEvent(){
        //전송 버튼
        Button confirmbtn = (Button)findViewById(R.id.dfconfirmbtn);
        confirmbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "전송");
                onClickConfirm(emailidText.getText().toString(),getFindDialog());
            }
        });

        //취소버튼
        Button cancelbtn = (Button)findViewById(R.id.dfcancelbtn);
        cancelbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "취소");
                onClickCancel();
                dismiss();
            }
        });
    }

    private FindPwDialog getFindDialog(){
        return this;
    }
}
