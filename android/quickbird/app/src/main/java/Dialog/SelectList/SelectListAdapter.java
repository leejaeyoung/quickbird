package Dialog.SelectList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.quickbird.quickbird.R;

/**
 * Created by KyoungSik on 2017-03-11.
 * SelectListDialog 리스트뷰 adapter
 */
public class SelectListAdapter extends ArrayAdapter{

    private int resource;
    private Activity act;
    private LayoutInflater mInflater;
    private String[] listtitle;



    public SelectListAdapter(Context context, int resource) {
        super(context, resource);
    }

    public SelectListAdapter(Context context, int resource, String[] listtitle){
        super(context, resource);
        this.resource = resource;
        this.act = (Activity)context;
        mInflater = (LayoutInflater)act.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.listtitle = listtitle;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public int getCount(){
        return listtitle.length;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        //  if(convertView == null) {
        convertView = mInflater.inflate(resource, null);
        TextView titleText = (TextView)convertView.findViewById(R.id.listtitle_cell);
        titleText.setText(listtitle[position]);
        //  }
       // convertView.setLayoutParams(layoutParams);
        return convertView;
    }
}
