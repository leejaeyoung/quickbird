package Dialog.SelectList;

import android.view.View;

import Dialog.SelectListDialog;

/**
 * Created by KyoungSik on 2017-03-11.
 * SelectListDialog 이벤트 리스너
 */
public interface OnSelectDialogListener {
    public void onClickCancel();
    public void onClickConfirm(SelectListDialog selectListDialog,int position,String[] titlelist);
    public void onClickList(int position,View view,String[] titlelist);
}
