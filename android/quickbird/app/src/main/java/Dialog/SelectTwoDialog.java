package Dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import com.quickbird.quickbird.R;

/**
 * Created by KyoungSik on 2017-03-06.
 * 2가지 선택 다이얼로그
 */
public abstract class SelectTwoDialog extends Dialog {

    private Button selectOneButton;//첫번쨰 버튼
    private Button selectTwoButton;//두번쨰 버튼
    private Button cancelbtn;//취소버튼

    abstract public void clickOneButton(SelectTwoDialog selectTwoDialog);
    abstract public void clickTwoButton(SelectTwoDialog selectTwoDialog);
    abstract public void clickCancelButton();

    public SelectTwoDialog(Context context) {
        super(context);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.setCanceledOnTouchOutside(false);
        setContentView(R.layout.dialog_select_user);
        init();
    }

    public void init(){
        selectOneButton = (Button)findViewById(R.id.select_btn1);
        selectOneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickOneButton(getSelectTwoDialog());
            }
        });
        selectTwoButton = (Button)findViewById(R.id.select_btn2);
        selectTwoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickTwoButton(getSelectTwoDialog());
            }
        });
        cancelbtn = (Button)findViewById(R.id.dsucancelbtn);
        cancelbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                clickCancelButton();
            }
        });
    }

    public Button getSelectOneButton() {
        return selectOneButton;
    }

    public Button getSelectTwoButton() {
        return selectTwoButton;
    }

    public SelectTwoDialog getSelectTwoDialog(){
        return this;
    }
}
