package Dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.quickbird.quickbird.R;

import java.util.ArrayList;

import Dialog.SelectList.OnSelectDialogListener;
import Dialog.SelectList.SelectListAdapter;

/**
 * Created by KyoungSik on 2017-03-11.
 * 리스트 뷰를 포함한 다이얼로그
 */
public class SelectListDialog extends Dialog {

    private final String TAG = "SelectListDialog";

    private OnSelectDialogListener onSelectDialogListener;

    private ListView listView;//정보 선택 창

    private Button cancelbtn;//취소 버튼
    private Button confirmbtn;//확인 버튼
    private View beforeView;//이전 선택한 리스트 뷰 아이템
    private TextView titleText;//다이얼로그 타이틀 TextView
    private Activity act;
    private String[] titlelist;
    private int currentPositon=0;

    public SelectListDialog(Context context,String[] titlelsit) {
        super(context);
        this.act = (Activity)context;
        this.titlelist = titlelsit;
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.setCanceledOnTouchOutside(false);
        setContentView(R.layout.dialog_select_list);
        init();
    }

    private void init(){
        titleText=(TextView)findViewById(R.id.dsltitleText);
        listView = (ListView)findViewById(R.id.sdlistview);

       /*ArrayList<String> test = new ArrayList<String>();
        for(int i = 0; i<10;i++){
           test.add(""+i);
        }*/
       // ArrayAdapter<String> adapter = new ArrayAdapter<String>(act, android.R.layout.simple_list_item_1, test) ;

        SelectListAdapter selectListAdapter = new SelectListAdapter(act,R.layout.selectlist_dialog_cell,titlelist);
        listView.setAdapter(selectListAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(beforeView != null){
                    beforeView.setBackgroundColor(Color.WHITE);
                }
                view.setBackgroundColor(Color.rgb(185,195,194));
                beforeView = view;
                currentPositon = position;

                if(onSelectDialogListener != null)
                    onSelectDialogListener.onClickList(position,view,titlelist);
            }
        });
        cancelbtn = (Button)findViewById(R.id.dslcancelbtn);
        cancelbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onSelectDialogListener != null)
                    onSelectDialogListener.onClickCancel();
                dismiss();
            }
        });

        confirmbtn = (Button)findViewById(R.id.dslconfirmbtn);
        confirmbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onSelectDialogListener != null)
                    onSelectDialogListener.onClickConfirm(getSelectListDialog(),currentPositon,titlelist);

            }
        });

    }

    public Button getConfirmbtn() {
        return confirmbtn;
    }

    public Button getCancelbtn() {
        return cancelbtn;
    }

    public void setOnSelectDialogListener(OnSelectDialogListener onSelectDialogListener) {
        this.onSelectDialogListener = onSelectDialogListener;
    }

    public SelectListDialog getSelectListDialog(){
        return this;
    }

    public View getBeforeView() {
        return beforeView;
    }

    public TextView getTitleText() {
        return titleText;
    }

    public String[] getTitlelist() {
        return titlelist;
    }
}
