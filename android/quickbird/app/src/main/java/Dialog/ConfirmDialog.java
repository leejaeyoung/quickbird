package Dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.quickbird.quickbird.R;

import org.w3c.dom.Text;

/**
 * Created by KyoungSik on 2017-03-09.
 * 확인 다이얼로그 창
 */
public abstract class ConfirmDialog extends Dialog {

    private final String TAG = "ConfirmDialog";

    abstract public void onClickConfirm(ConfirmDialog confirmDialog);//확인 클릭 했을시
    abstract public void onClickCancel();//취소 클릭했을시

    private Button confirmBtn;
    private Button cancelBtn;
    private TextView titleText;

    public ConfirmDialog(Context context) {
        super(context);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.setCanceledOnTouchOutside(false);
        setContentView(R.layout.dialog_confirm);
        init();
        buttonEvent();
    }

    private void init(){
        confirmBtn = (Button)findViewById(R.id.cdconfirmbtn);
        cancelBtn = (Button)findViewById(R.id.cdcancelbtn);
        titleText = (TextView)findViewById(R.id.cdconfirmText);
    }

    private void buttonEvent(){
        confirmBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickConfirm(getConfirmDialog());
            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickCancel();
                dismiss();
            }
        });
    }

    public Button getConfirmBtn() {
        return confirmBtn;
    }

    public Button getCancelBtn() {
        return cancelBtn;
    }

    public TextView getTitleText() {
        return titleText;
    }

    public ConfirmDialog getConfirmDialog(){
        return this;
    }
}
