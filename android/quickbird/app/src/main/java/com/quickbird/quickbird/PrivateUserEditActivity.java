package com.quickbird.quickbird;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import java.util.ArrayList;

import Register.ImageIcon;

/**
 * Created by KyoungSik on 2017-03-13.
 * 개인 회원 정보수정
 */
public class PrivateUserEditActivity extends Activity {

    private final String TAG = "PrivateUserEditActivity";

    public final int ICON_EMAIL = 0;
    public final int ICON_NAME = 1;
    public final int ICON_PHONE = 2;


    private ArrayList<ImageIcon> imageIcons = new ArrayList<ImageIcon>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_private_userinfo_edit);
        init();
    }

    private void init(){
        imageIconinit();
        buttonEvent();
        editTextEvent();
    }

    private void buttonEvent(){
        Button infoeditbtn = (Button)findViewById(R.id.pueInfoeditbtn);
        infoeditbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "정보수정");
            }
        });

        Button cancelbtn = (Button)findViewById(R.id.pueCancelbtn);
        cancelbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "취소");
                finish();
            }
        });
    }

    /* 아이콘 초기화
  *
  * */
    private void imageIconinit(){

        //이메일 아이디 아이콘
        ImageIcon emaildIcon = new ImageIcon();
        emaildIcon.setEditText((EditText) findViewById(R.id.pueEmailidText));
        emaildIcon.setView((ImageView) findViewById(R.id.pueEmailidImage));
        emaildIcon.setImageId(ICON_EMAIL);
        emaildIcon.setSetOnImage(R.drawable.icon_email_on);
        emaildIcon.setSetOffImage(R.drawable.icon_email_off);
        imageIcons.add(emaildIcon);

        //이름  아이콘
        ImageIcon nameIcon = new ImageIcon();
        ImageView nameImage = (ImageView)findViewById(R.id.pueNameImage);
        nameIcon.setView(nameImage);
        nameIcon.setEditText((EditText) findViewById(R.id.pueNameText));
        nameIcon.setImageId(ICON_NAME);
        nameIcon.setSetOnImage(R.drawable.icon_user_on);
        nameIcon.setSetOffImage(R.drawable.icon_user_off);
        imageIcons.add(nameIcon);

        //폰 아이콘
        ImageIcon imageIcon = new ImageIcon();
        ImageView phoneIconImage = (ImageView)findViewById(R.id.puePhoneImage);
        imageIcon.setView(phoneIconImage);
        imageIcon.setEditText((EditText) findViewById(R.id.puePhoneText));
        imageIcon.setImageId(ICON_PHONE);
        imageIcon.setSetOnImage(R.drawable.icon_phone_on);
        imageIcon.setSetOffImage(R.drawable.icon_phone_off);
        imageIcons.add(imageIcon);

    }

    /* EditText 포커스 바꼈을시 아이콘 이미지 상태 바꿈
    *
    * */
    private void editTextEvent(){

        for(int i=0;i<imageIcons.size();i++) {
            imageIcons.get(i).getEditText().setId(imageIcons.get(i).getImageId());
            imageIcons.get(i).getEditText().setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (hasFocus) {
                        imageIcons.get(v.getId()).getView().setBackgroundResource(imageIcons.get(v.getId()).getSetOnImage());
                    } else {
                        imageIcons.get(v.getId()).getView().setBackgroundResource(imageIcons.get(v.getId()).getSetOffImage());
                    }
                }
            });
        }
    }

}
