package com.quickbird.quickbird;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebView;
import android.widget.LinearLayout;

import Dialog.Loading;
import Dialog.SelectList.OnSelectDialogListener;
import Dialog.SelectListDialog;
import Dialog.SelectTwoDialog;
import QB_MainVIewPager.BookMarkPage.OnBookMarkPageListener;
import QB_MainVIewPager.HomePage.OnHomePageListener;
import QB_MainVIewPager.MyPage.MyPageListView;
import QB_MainVIewPager.MyPage.OnMyListInfoListener;
import QB_MainVIewPager.QB_ViewPageAdapter;
import QB_MainVIewPager.Qb_ViewPageTab;
import QB_MainVIewPager.SurroundingDeliveryPage.OnSurroundingDeliveryListener;
import QB_MainVIewPager.SurroundingDeliveryPage.SurroundingDeliveryPageView;
import SearchLayout.SearchLayout;
import SearchLayout.OnSearchLayoutListener;
import connection.OnWebViewClientListener;
import memory.RecycleUtils;

/**
 * Created by KyoungSik on 2017-03-03.
 * 메인 페이지
 */
public class QB_MainActivity extends Activity {

    private final String TAG = "QB_MainActivity";

    private ViewPager pager;//메인 뷰 페이지
    private QB_ViewPageAdapter qb_viewPageAdapter;//메인 뷰 페이지 어뎁터
    private Qb_ViewPageTab qb_viewPageTab;//메인 뷰페이지 탭

    private DrawerLayout drawerLayout;//홈 페이지 레이아웃
    private LinearLayout leftRL;//홈페이지 검색 필터 레이아웃

    private LinearLayout rightRL;//즐겨찾기 검색 필터 레이아웃

    private int statePage;//현재 페이지 상태
    private boolean startgetData = true;

    private Loading loading;//로딩 다이얼로그


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qb_main);

        //로딩 시작
        loading = new Loading(this);
        loading.show();

        init();
    }
    /*초기화
    *
    * */
    private void init(){
        leftRL = (LinearLayout)findViewById(R.id.searchLayout);
        rightRL = (LinearLayout)findViewById(R.id.filterLayout);
        drawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);



        qb_viewPageTab = new Qb_ViewPageTab(this) {
            @Override
            public void touchListenear(int underbtn, Qb_ViewPageTab qb_viewpageTab) {
                pager.setCurrentItem(underbtn);
                qb_viewpageTab.setBtnImage(underbtn);
            }
        };
        initViewPager();
        qb_viewPageTab.setBtnImage(pager.getCurrentItem());

        //홈 페이지의 검색 사이드메뉴
        SearchLayout searchLayout = new SearchLayout(leftRL,this);
        searchLayout.setOnSearchLayoutListener(new OnSearchLayoutListener() {
            @Override
            public void onClickSearchState(int state) {
                Log.d(TAG,"onClickSearchState : " + state);
            }

            @Override
            public void onClickItem(int position, String[] titlelist) {
                Log.d(TAG, "titllist : " + titlelist[position]);
            }

            @Override
            public void onClickSearchBtn() {

            }

            @Override
            public void onClickCancelBtn() {
                Log.d(TAG,"Home onClickCancelBtn");
                onCloseLeftDrawer();//홈페이지 검색 창 닫기
            }
        });

        //즐겨찾기 페이지 필터 사이트메뉴
        SearchLayout filterLayout = new SearchLayout(rightRL,this);
        filterLayout.setOnSearchLayoutListener(new OnSearchLayoutListener() {
            @Override
            public void onClickSearchState(int state) {
                Log.d(TAG,"onClickSearchState : " + state);
            }

            @Override
            public void onClickItem(int position, String[] titlelist) {
                Log.d(TAG, "titllist : " + titlelist[position]);
            }

            @Override
            public void onClickSearchBtn() {

            }

            @Override
            public void onClickCancelBtn() {
                Log.d(TAG,"BookMark onClickCancelBtn");
                onCloseRightDrawer();// 즐겨찾기 페이지 검색 창 닫기
            }
        });
    }

    /*뷰페이지 초기화
    *
    * */
    private void initViewPager(){
        pager = (ViewPager)findViewById(R.id.pager);
        qb_viewPageAdapter = new QB_ViewPageAdapter(getLayoutInflater(),this);
        pager.setAdapter(qb_viewPageAdapter);
        pager.setOffscreenPageLimit(4);
        pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                // underbar.setStateUnderbar(position);
                if (startgetData) {
                    statePage = qb_viewPageAdapter.pageState(0);
                    startgetData = false;
                    initHomePageEvent();//홈 페이지뷰 이벤트
                    initBookMarkPageEvent();//즐겨찾기 페이지뷰 이벤트
                    initMyPageEvent();//마이페이지 이벤트
                    initSurroundingDeliveryPageEvent();//주변 배송자 페이지 이벤트
                    searchState(position);//사이드 레이아웃 상태

                }
               // checkMyPage(position);
            }

            @Override
            public void onPageSelected(int position) {
                qb_viewPageTab.setBtnImage(position);
                statePage = qb_viewPageAdapter.pageState(position);
                searchState(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
              //  checkMyPage(state);
            }
        });
    }

    /* 현재 페이지 상태가 마이페이지 일떄
    *  position : 현재 뷰페이지의 페이지
    * */
   /* public void checkMyPage(int position){
        if(qb_viewPageAdapter.myPage == position){// 마이페이지 상태일떄
            qb_viewPageAdapter.getMyPageView().getMyListInfoView().setChangedinit();//버튼 배경 초기화
        }
    }*/

    /* 마이페이지 이벤트
    *
    * */
    private void initMyPageEvent(){

        qb_viewPageAdapter.getMyPageView().getMyPageListView().setOnMyListInfoListener(new OnMyListInfoListener() {
            @Override
            public void onClickListItem(View v, int state, MyPageListView myPageListView) {
                //  v.setBackgroundColor(Color.rgb(185,195,194));
                if (myPageListView.USER_INFO_EDIT == state) {
                    Intent intent = new Intent(QB_MainActivity.this, CompanyUserEditActivity.class);
                    startActivity(intent);
                }else if(myPageListView.MY_REGISTERED_FREIGHT == state){
                    Intent intent = new Intent(QB_MainActivity.this, MyRegisteredFreightActivity.class);
                    startActivity(intent);
                }else if(myPageListView.MY_REGISTERED_DELIVERY == state){
                    Intent intent = new Intent(QB_MainActivity.this, MyRegisteredDeliveryActivity.class);
                    startActivity(intent);
                }
            }
        });

       /* qb_viewPageAdapter.getMyPageView().getMyListInfoView().setOnMyListInfoListener(new OnMyListInfoListener() {
            @Override
            public void onClickListUp(View v, int state) {
                if (qb_viewPageAdapter.getMyPageView().getMyListInfoView().USER_INFO_EDIT == state) {
                    Log.d(TAG, "USER_INFO_EDIT");
                    v.setBackgroundColor(Color.WHITE);
                } else if (qb_viewPageAdapter.getMyPageView().getMyListInfoView().MY_REGISTERED_FREIGHT == state) {
                    Log.d(TAG, "MY_REGISTERED_FREIGHT");
                    v.setBackgroundColor(Color.WHITE);
                } else if (qb_viewPageAdapter.getMyPageView().getMyListInfoView().MY_REGISTERED_DELIVERY == state) {
                    Log.d(TAG, "MY_REGISTERED_DELIVERY");
                    v.setBackgroundColor(Color.WHITE);
                }
            }

            @Override
            public void onClickListDown(View v, int state) {
                if (qb_viewPageAdapter.getMyPageView().getMyListInfoView().USER_INFO_EDIT == state) {
                    Log.d(TAG, "USER_INFO_EDIT");
                    v.setBackgroundColor(Color.rgb(185, 195, 194));
                } else if (qb_viewPageAdapter.getMyPageView().getMyListInfoView().MY_REGISTERED_FREIGHT == state) {
                    Log.d(TAG, "MY_REGISTERED_FREIGHT");
                    v.setBackgroundColor(Color.rgb(185, 195, 194));
                } else if (qb_viewPageAdapter.getMyPageView().getMyListInfoView().MY_REGISTERED_DELIVERY == state) {
                    Log.d(TAG, "MY_REGISTERED_DELIVERY");
                    v.setBackgroundColor(Color.rgb(185, 195, 194));
                }
            }
        });*/
    }

    /* 즐겨찾기 이벤트
    *
    * */
    private void initBookMarkPageEvent(){
        qb_viewPageAdapter.getBookMarkPageView().setOnBookMarkPageListener(new OnBookMarkPageListener() {
            @Override
            public void onClickfilter() {
                onOpenRightDrawer();
            }
        });

        qb_viewPageAdapter.getBookMarkPageView().getBookMarkPageWebView().getWebViewClientClass().setOnWebViewClientListener(new OnWebViewClientListener() {
            @Override
            public void startWebClient(WebView webView, String url) {

            }

            @Override
            public void finishWebClient(WebView webView, String url) {
                qb_viewPageAdapter.getBookMarkPageView().getBookMarkPageWebView().setWebLoadFinish(true);//즐겨찾기 웹 페이지 load 완료
                if (finishLoadPage()) {
                    loading.dismiss();
                }
            }

            @Override
            public void errorWebClient(WebView webView, int errorCode, String failingUrl) {
                loading.dismiss();
            }
        });
    }

    /* 홈 페이지 이벤트
    *
    * */
    private void initHomePageEvent(){
        qb_viewPageAdapter.getHomePageView().setOnHomePageListener(new OnHomePageListener() {
            @Override
            public void onClickSearch() {
                onOpenLeftDrawer();
            }

            @Override
            public void onClickWrite() {
                //화물등록 및 배송자등록 선택 다이얼로그
                SelectTwoDialog selectTwoDialog = new SelectTwoDialog(QB_MainActivity.this) {
                    @Override
                    public void clickOneButton(SelectTwoDialog selectTwoDialog) {
                        Log.d(TAG, "화물 등록하기");
                        Intent private_user = new Intent(QB_MainActivity.this, FreightRegisterActivity.class);
                        startActivity(private_user);
                        selectTwoDialog.dismiss();

                    }

                    @Override
                    public void clickTwoButton(SelectTwoDialog selectTwoDialog) {
                        Log.d(TAG, "배송자 등록하기");
                        Intent company_user = new Intent(QB_MainActivity.this, DeliveryRegisterActivity.class);
                        startActivity(company_user);
                        selectTwoDialog.dismiss();

                    }

                    @Override
                    public void clickCancelButton() {

                    }
                };
                selectTwoDialog.getSelectOneButton().setText("화물 등록하기");
                selectTwoDialog.getSelectTwoButton().setText("배송자 등록하기");
                selectTwoDialog.show();
            }
        });

        qb_viewPageAdapter.getHomePageView().getHomePageWebView().getWebViewClientClass().setOnWebViewClientListener(new OnWebViewClientListener() {
            @Override
            public void startWebClient(WebView webView, String url) {

            }

            @Override
            public void finishWebClient(WebView webView, String url) {
                qb_viewPageAdapter.getHomePageView().getHomePageWebView().setWebLoadFinish(true);//홈 페이지의 웹 뷰 load 완료
                if(finishLoadPage()){
                    loading.dismiss();
                }

            }

            @Override
            public void errorWebClient(WebView webView, int errorCode, String failingUrl) {
                loading.dismiss();
            }
        });

    }

    /* 주변 배송자 이벤트
    *
    * */
    public void initSurroundingDeliveryPageEvent(){
        qb_viewPageAdapter.getSurroundingDeliveryPageView().setOnSurroundingDeliveryListener(new OnSurroundingDeliveryListener() {
            @Override
            public void onClickSearch(SurroundingDeliveryPageView surroundingDeliveryPageView) {
                Log.d(TAG, "initSurroundingDeliveryPageEvent onClickSearch");
            }

            @Override
            public void onClickReturn(SurroundingDeliveryPageView surroundingDeliveryPageView) {
                Log.d(TAG, "initSurroundingDeliveryPageEvent onClickReturn");
            }
        });

        qb_viewPageAdapter.getSurroundingDeliveryPageView().getSurroundingDeliveryWebView().getWebViewClientClass().setOnWebViewClientListener(new OnWebViewClientListener() {
            @Override
            public void startWebClient(WebView webView, String url) {

            }

            @Override
            public void finishWebClient(WebView webView, String url) {
                qb_viewPageAdapter.getSurroundingDeliveryPageView().getSurroundingDeliveryWebView().setWebLoadFinish(true);//즐겨찾기 웹 페이지 load 완료
                if(finishLoadPage()){
                    loading.dismiss();
                }
            }

            @Override
            public void errorWebClient(WebView webView, int errorCode, String failingUrl) {

            }
        });
    }

    /* 왼쪽 사이드 검색 레이아웃 홈 페이지일때만 적용
    *
    * */
    public void searchState(int position){
        if(qb_viewPageAdapter.homePage==position){//홈페이지일 겡우
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED, Gravity.LEFT);//홈 페이지 왼쪽 사이드 필터 레이아웃 이용
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED,Gravity.RIGHT);//즐겨찾기 오른쪽 사이드 필터 레이아웃 이용하지 못함
        }else if(qb_viewPageAdapter.bookmarkPage == position){//즐겨찾기일 경우
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED, Gravity.RIGHT);//즐겨찾기 오른쪽 사이드 필터 레이아웃 이용
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED,Gravity.LEFT);//홈 페이지 왼쪽 사이드 필터 레이아웃 이용하지 못함
        }else{//나머지 페이지일 경우
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);//오른쪽, 왼쪽 사이드 필터 다이용 못함
        }
    }

    public boolean finishLoadPage(){
        boolean finishLoad = false;
        if(!qb_viewPageAdapter.getHomePageView().getHomePageWebView().isWebLoadFinish()){
            Log.d(TAG, "홈 페이지 load 안됨");
            finishLoad = false;
        }else if(!qb_viewPageAdapter.getBookMarkPageView().getBookMarkPageWebView().isWebLoadFinish()){
            Log.d(TAG, "즐겨찾기 페이지 load 안됨");
            finishLoad = false;
        }else if(!qb_viewPageAdapter.getSurroundingDeliveryPageView().getSurroundingDeliveryWebView().isWebLoadFinish()){
            Log.d(TAG,"주변배송자 페이지 load 안됨");
            finishLoad = false;
        }else{
            finishLoad = true;// 모든 웹 load 완료료
        }
       return finishLoad;
    }

    /* 홈 페이지의 왼쪽 사이드 검색 필터 레이아웃 열리는 에니메이션
 *
 * */
    public void onOpenLeftDrawer()
    {
        drawerLayout.openDrawer(leftRL);
    }

    /* 홈 페이지의 왼쪽 사이드 검색 필터 레이아웃 닫히는 애니메이션
*
* */
    public void onCloseLeftDrawer(){
        drawerLayout.closeDrawer(leftRL);
    }

    /* 즐겨찾기 페이지의 오른쪽 검색 필터 레이아웃 열리는 애니메이션
    *
   * */
    public void onOpenRightDrawer(){
        drawerLayout.openDrawer(rightRL);
    }
    /* 즐겨찾기 페이지의 오른쪽 검색 필터 레이아웃 닫히는 애니메이션
  *
 * */
    public void onCloseRightDrawer(){
        drawerLayout.closeDrawer(rightRL);
    }
    @Override
    protected void onDestroy(){
        super.onDestroy();
        Log.d(TAG, "destory");
        RecycleUtils.recursiveRecycle(getWindow().getDecorView());
    }
}
