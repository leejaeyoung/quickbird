package com.quickbird.quickbird;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

import Dialog.FindPwDialog;
import Dialog.SelectTwoDialog;
import Register.ImageIcon;
import memory.RecycleUtils;

/**
 * Created by KyoungSik on 2017-03-02
 * 로그인 페이지
 */
public class LoginActivity extends Activity {

    private final String TAG = "LoginActivity";

    private boolean autologinState = false;

    public final int ICON_EMAIL = 0;//이메일 아이디
    public final int ICON_PW = 1;//비밀번호

    private ArrayList<ImageIcon> imageIcons = new ArrayList<ImageIcon>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        init();
    }


    private void init(){
        imageIconinit();
        buttonEvent();
        editTextEvent();
    }

    /* 로그인 페이지 버튼 이벤트
    *
    * */
    private void buttonEvent(){
        //로그인 버튼
        Button loginbtn = (Button)findViewById(R.id.loginbtn);
        loginbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "login button click");
                Intent intent = new Intent(LoginActivity.this,QB_MainActivity.class);
                startActivity(intent);
            }
        });

        //회원가입
        TextView register = (TextView)findViewById(R.id.lregisterbtn);
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SelectTwoDialog selectTwoDialog = new SelectTwoDialog(LoginActivity.this) {
                    @Override
                    public void clickOneButton(SelectTwoDialog selectTwoDialog) {//개인 회원가입 버튼 클릭
                        Log.d(TAG,"개인 회원");
                        Intent private_user = new Intent(LoginActivity.this,PrivateUserRegisterActivity.class);
                        startActivity(private_user);
                        selectTwoDialog.dismiss();
                    }

                    @Override
                    public void clickTwoButton(SelectTwoDialog selectTwoDialog) {//사업자 회원가입 버튼 클릭
                        Log.d(TAG,"사업자 회원");
                        Intent company_user = new Intent(LoginActivity.this,CompanyUserRegisterActivity.class);
                        startActivity(company_user);
                        selectTwoDialog.dismiss();
                    }

                    @Override
                    public void clickCancelButton() {

                    }
                };
                selectTwoDialog.getSelectOneButton().setText("개인 회원가입");
                selectTwoDialog.getSelectTwoButton().setText("사업자 회원가입");
                selectTwoDialog.show();
            }
        });

        //비밀번호찾기
        TextView findPwText = (TextView)findViewById(R.id.findpwText);
        findPwText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FindPwDialog findPwDialog = new FindPwDialog(LoginActivity.this) {
                    @Override
                    public void onClickConfirm(String emailid, FindPwDialog findPwDialog) {
                        Log.d(TAG, "emaild : " + emailid);
                        findPwDialog.dismiss();
                    }

                    @Override
                    public void onClickCancel() {

                    }
                };
                findPwDialog.show();
            }
        });

        Button autobtn = (Button)findViewById(R.id.autologinbtn);
        autobtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!autologinState) {
                    autologinState = true;
                    v.setBackgroundResource(R.drawable.icon_check_on);
                } else {
                    autologinState = false;
                    v.setBackgroundResource(R.drawable.icon_check_off);
                }
            }
        });
    }

    /* EditText 포커스 바꼈을시 아이콘 이미지 상태 바꿈
*
* */
    private void editTextEvent(){
        for(int i=0;i<imageIcons.size();i++) {
            imageIcons.get(i).getEditText().setId(imageIcons.get(i).getImageId());
            imageIcons.get(i).getEditText().setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (hasFocus) {
                        imageIcons.get(v.getId()).getView().setBackgroundResource(imageIcons.get(v.getId()).getSetOnImage());
                    } else {
                        imageIcons.get(v.getId()).getView().setBackgroundResource(imageIcons.get(v.getId()).getSetOffImage());
                    }
                }
            });
        }
    }

    /* 아이콘 초기화
    *
    * */
    private void imageIconinit() {

        //이메일 아이디 아이콘
        ImageIcon emaildIcon = new ImageIcon();
        emaildIcon.setEditText((EditText) findViewById(R.id.lemailidText));
        emaildIcon.setImageId(ICON_EMAIL);
        emaildIcon.setView((ImageView) findViewById(R.id.lemailidImage));
        emaildIcon.setSetOnImage(R.drawable.icon_email_on);
        emaildIcon.setSetOffImage(R.drawable.icon_email_off);
        imageIcons.add(emaildIcon);

        //비밀번호  아이콘
        ImageIcon pwIcon = new ImageIcon();
        ImageView pwImage = (ImageView) findViewById(R.id.lpwImage);
        pwIcon.setView(pwImage);
        pwIcon.setEditText((EditText) findViewById(R.id.lpwText));
        pwIcon.setImageId(ICON_PW);
        pwIcon.setSetOnImage(R.drawable.icon_lock_on);
        pwIcon.setSetOffImage(R.drawable.icon_lock_off);
        imageIcons.add(pwIcon);
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        Log.d(TAG, "destory");
        RecycleUtils.recursiveRecycle(getWindow().getDecorView());
    }
}
