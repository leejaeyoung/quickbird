package com.quickbird.quickbird;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Display;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import memory.RecycleUtils;

/**
 * Created by KyoungSik on 2017-03-02.
 * 인트로 페이지
 */
public class IntroActivity extends Activity {
    private final String TAG = "IntroActivity";

    private LinearLayout logo_linear;//움직이는 로고
    private ImageView introimageView;
    private ImageView logoimageView;//배경 로고

    private int fromWidth = 0;


    private TimerTask mTask;
    private Timer mTimer;

    private int logoAlpha = 0;//배경로고 현재 알파값
    private final int MAXAlpha = 200;//배경 로고 최대 알파값

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);
        //initAnimation();
        initAnimation();
        initLogoAnimation();
//        Log.d(TAG, "Activity size : " + introimageView.getMaxWidth());
      // onNextPage();
    }

    /* 배경로고가 점점보이는 애니메이션
    *
    * */
    private void initLogoAnimation(){
        logoimageView = (ImageView)findViewById(R.id.background_logo);
        /*Animation anim = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.alpha_anim);
        logoimageView.startAnimation(anim);*/
        mTask = new TimerTask() {
            @Override
            public void run() {
                new GetData().execute();

            }
        };
        mTimer = new Timer();
        mTimer.schedule(mTask, 0, 100);//0.1초당 한번씩 이미지 바꿈*/

    }

    /* 움직이는 로고가 움직이는 에니메이션
    *
    * */
    private void initAnimation(){
        logo_linear = (LinearLayout)findViewById(R.id.logo_layout);
        introimageView = (ImageView)findViewById(R.id.introImageView);


        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        fromWidth = size.x;

        movewAnimation(logo_linear);
    }

    /* 다음 페이지로 넘어감
    *
    * */
    private void onNextPage(){
        Handler hd = new Handler();
        hd.postDelayed(new Runnable() {
            @Override
            public void run() {
                IntroActivity.this.startActivity(new Intent(IntroActivity.this, LoginActivity.class));
                IntroActivity.this.finish();
            }
        }, 2000);
    }

    /* 레이아웃 에니메이션 효과
    *
    * */
    public void movewAnimation(LinearLayout linearLayout){
        TranslateAnimation move = new TranslateAnimation
                (0,   // fromXDelta
                        fromWidth+100,  // toXDelta
                        0,    // fromYDelta
                        0);// toYDelta
        move.setDuration(1000);
        move.setFillAfter(true);
        linearLayout.startAnimation(move);

    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        Log.d(TAG, "destory");
        RecycleUtils.recursiveRecycle(getWindow().getDecorView());
    }

    /*
  * 서버의 정보를 받아오기 위한 비동기 스레드 inner클래스
  * */
    public class GetData extends AsyncTask<String, ArrayList<String>, String> {


        public GetData(){

        }
        public GetData(String urlStr,String body){

        }
        @Override
        protected void onCancelled() {
            super.onCancelled();
        }


        @Override
        protected String doInBackground(String... params) {
            String data = "";
            logoAlpha = logoAlpha + 10;
            if(logoAlpha >= MAXAlpha){
                logoAlpha = MAXAlpha;
            }
            return data;
        }
        @Override
        protected void onPostExecute(String result){
            super.onPostExecute(result);

            Drawable logoAlphaD = logoimageView.getBackground();
            logoAlphaD.setAlpha(logoAlpha);
            if(logoAlpha == MAXAlpha){
                mTimer.cancel();
                onNextPage();

            }
            Log.d(TAG, "logoAlpha : " + logoAlpha);

        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();


        }

        @Override
        protected void onProgressUpdate(ArrayList<String>... values) {
            super.onProgressUpdate(values);

        }
    }
}
