package com.quickbird.quickbird;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.widget.Button;

import java.util.ArrayList;

import OptionGuidePageViewPager.Og_ViewPageAdapter;
import OptionGuidePageViewPager.OptionGuideTab;
import QB_MainVIewPager.QB_ViewPageAdapter;
import memory.RecycleUtils;

/**
 * Created by KyoungSik on 2017-03-09.
 * 옵션 가이드
 */
public class OptionGuideActivity extends Activity {

    private final String TAG = "OptionGuideActivity";

    private ViewPager viewPager;
    private OptionGuideTab optionGuideTab;
    private Og_ViewPageAdapter og_viewPageAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_optionguide);

        initViewPager();
        initOptionGuideTab();

    }

    /* 설정 가이드 뷰페이저 셋팅
    *
    * */
    private void initViewPager(){

        int[] imageResource = new int[3];
        imageResource[0] = R.drawable.quickbird_intro;
        imageResource[1] = R.drawable.quickbird_intro;
        imageResource[2] = R.drawable.quickbird_intro;


        viewPager = (ViewPager)findViewById(R.id.optionguideViewPager);
        og_viewPageAdapter = new Og_ViewPageAdapter(getLayoutInflater(),this,imageResource);
        viewPager.setAdapter(og_viewPageAdapter);
        viewPager.setOffscreenPageLimit(4);
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                optionGuideTab.setSelectTab(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                //initOptionGuideTab(state);
            }
        });

    }

    /* 설정가이드 페이지 탭 셋팅
    *
    * */
    private void initOptionGuideTab(){
        optionGuideTab = new OptionGuideTab(this) {
            @Override
            public void onClickOptionBtn(int position, OptionGuideTab optionGuideTab) {
                optionGuideTab.setSelectTab(position);
                viewPager.setCurrentItem(position);
            }

        };
        optionGuideTab.setSelectTab(viewPager.getCurrentItem());
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        RecycleUtils.recursiveRecycle(getWindow().getDecorView());
    }

}
