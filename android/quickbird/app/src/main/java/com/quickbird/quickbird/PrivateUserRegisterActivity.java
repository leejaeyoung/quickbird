package com.quickbird.quickbird;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import Dialog.ConfirmDialog;
import Register.ImageIcon;
import memory.RecycleUtils;

/**
 * Created by KyoungSik on 2017-03-06.
 * 개인 회원가입
 */
public class PrivateUserRegisterActivity extends Activity {

    private final String TAG = "PrivateUserRegister";

    public final int ICON_EMAIL = 0;
    public final int ICON_PW = 1;
    public final int ICON_NAME = 2;
    public final int ICON_PHONE = 3;
    public final int ICON_CERTIFY = 4;

    private ArrayList<ImageIcon> imageIcons = new ArrayList<ImageIcon>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_privateuser_register);
        init();
    }

    private void init(){
        imageIconinit();
        buttonEvent();
        editTextEvent();
    }

    /* 아이콘 초기화
    *
    * */
    private void imageIconinit(){

        //이메일 아이디 아이콘
        ImageIcon emaildIcon = new ImageIcon();
        emaildIcon.setEditText((EditText)findViewById(R.id.pEmailidText));
        emaildIcon.setImageId(ICON_EMAIL);
        emaildIcon.setView((ImageView)findViewById(R.id.pEmailIconimage));
        emaildIcon.setSetOnImage(R.drawable.icon_email_on);
        emaildIcon.setSetOffImage(R.drawable.icon_email_off);
        imageIcons.add(emaildIcon);

        //비밀번호  아이콘
        ImageIcon pwIcon = new ImageIcon();
        ImageView pwImage = (ImageView)findViewById(R.id.pPwIconimage);
        pwIcon.setView(pwImage);
        pwIcon.setEditText((EditText)findViewById(R.id.pPwText));
        pwIcon.setImageId(ICON_PW);
        pwIcon.setSetOnImage(R.drawable.icon_lock_on);
        pwIcon.setSetOffImage(R.drawable.icon_lock_off);
        imageIcons.add(pwIcon);

        //이름  아이콘
        ImageIcon nameIcon = new ImageIcon();
        ImageView nameImage = (ImageView)findViewById(R.id.pNameIconimage);
        nameIcon.setView(nameImage);
        nameIcon.setEditText((EditText)findViewById(R.id.pNameText));
        nameIcon.setImageId(ICON_NAME);
        nameIcon.setSetOnImage(R.drawable.icon_user_on);
        nameIcon.setSetOffImage(R.drawable.icon_user_off);
        imageIcons.add(nameIcon);

        //폰 아이콘
        ImageIcon imageIcon = new ImageIcon();
        ImageView phoneIconImage = (ImageView)findViewById(R.id.pPhoneIconImage);
        imageIcon.setView(phoneIconImage);
        imageIcon.setEditText((EditText)findViewById(R.id.pPhoneText));
        imageIcon.setImageId(ICON_PHONE);
        imageIcon.setSetOnImage(R.drawable.icon_phone_on);
        imageIcon.setSetOffImage(R.drawable.icon_phone_off);
        imageIcons.add(imageIcon);

        //인증번호  아이콘
        ImageIcon certifyIcon = new ImageIcon();
        ImageView certifyImage = (ImageView)findViewById(R.id.pCertifyIconimage);
        certifyIcon.setView(certifyImage);
        certifyIcon.setEditText((EditText)findViewById(R.id.pCertifiyNumberText));
        certifyIcon.setImageId(ICON_CERTIFY);
        certifyIcon.setSetOnImage(R.drawable.icon_phone_on);
        certifyIcon.setSetOffImage(R.drawable.icon_phone_off);
        imageIcons.add(certifyIcon);
    }

    /* EditText 포커스 바꼈을시 아이콘 이미지 상태 바꿈
    *
    * */
    private void editTextEvent(){
        for(int i=0;i<imageIcons.size();i++) {
            imageIcons.get(i).getEditText().setId(imageIcons.get(i).getImageId());
            imageIcons.get(i).getEditText().setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (hasFocus) {
                        imageIcons.get(v.getId()).getView().setBackgroundResource(imageIcons.get(v.getId()).getSetOnImage());
                    } else {
                        imageIcons.get(v.getId()).getView().setBackgroundResource(imageIcons.get(v.getId()).getSetOffImage());
                    }
                }
            });
        }
    }

    /* EditText 예외처리
    *
    * */
    private boolean exception(){
        boolean check = true;
        for(int i=0;i<imageIcons.size();i++){
            if(imageIcons.get(i).getEditText().getText().toString().matches("")){
                check = false;
                imageIcons.get(i).getEditText().requestFocus();
                break;
            }
        }
        return check;
    }

    private boolean editTextException(EditText editText){
        boolean check = true;


        return check;
    }

    /* 버튼 이벤트
    *
    * */
    public void buttonEvent(){
        //취소버튼
        Button cancelbtn = (Button)findViewById(R.id.pCancelbtn);
        cancelbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConfirmDialog confirmDialog = new ConfirmDialog(getPrivateUserRegisterActivity()) {
                    @Override
                    public void onClickConfirm(ConfirmDialog confirmDialog) {
                        confirmDialog.dismiss();
                        getPrivateUserRegisterActivity().finish();
                    }

                    @Override
                    public void onClickCancel() {

                    }
                };
                confirmDialog.getConfirmBtn().setText("확인");
                confirmDialog.getCancelBtn().setText("취소");
                confirmDialog.getTitleText().setText("정말로 취소를 하시겠습니까?");
                confirmDialog.show();
            }
        });

        //회원가입 버튼
        Button registerbtn = (Button)findViewById(R.id.pRegisterbtn);
        registerbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(exception()){
                    Log.d(TAG,"예외처리");
               }else{
                    Log.d(TAG,"예외");
                }
            }
        });
    }

    public PrivateUserRegisterActivity getPrivateUserRegisterActivity(){
        return this;
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        RecycleUtils.recursiveRecycle(getWindow().getDecorView());
    }

}
