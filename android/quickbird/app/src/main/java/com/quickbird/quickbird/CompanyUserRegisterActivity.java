package com.quickbird.quickbird;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import java.util.ArrayList;

import Dialog.ConfirmDialog;
import Register.ImageIcon;
import memory.RecycleUtils;

/**
 * Created by KyoungSik on 2017-03-06.
 * 사업자 회원가입
 */
public class CompanyUserRegisterActivity extends Activity {
    private final String TAG = "CompanyUserRegisterActivity";

    public final int ICON_EMAIL = 0;//이메일 아이디
    public final int ICON_PW = 1;//비밀번호
    public final int ICON_CNAME = 2;//업체명
    public final int ICON_CEO = 3;//대표자
    public final int ICON_CIDENTITY = 4;//사업자등록번호
    public final int ICON_TYPE = 5;//업종
    public final int ICON_BUSINESS = 6;//업태
    public final int ICON_ADRESS = 7;//사업장 주소
    public final int ICON_PHONE = 8;//휴대폰
    public final int ICON_PCERTIFY = 9;//인증번호


    private ArrayList<ImageIcon> imageIcons = new ArrayList<ImageIcon>();//이미지 아이콘 관리

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_companyuser_register);
        init();
    }

    private void init(){
        imageIconinit();//이미지 아이콘 구조체 초기화
        buttonEvent();//버튼 이벤트
        editTextEvent();//EditText 포커스 이벤트
    }

    private void imageIconinit(){

        //이메일 아이디
        ImageIcon emaildIcon = new ImageIcon();
        emaildIcon.setEditText((EditText)findViewById(R.id.cEmailidText));
        emaildIcon.setView((ImageView) findViewById(R.id.cEmailidImage));
        emaildIcon.setImageId(ICON_EMAIL);
        emaildIcon.setSetOnImage(R.drawable.icon_email_on);
        emaildIcon.setSetOffImage(R.drawable.icon_email_off);
        imageIcons.add(emaildIcon);

        //비밀번호
        ImageIcon pwIcon = new ImageIcon();
        pwIcon.setEditText((EditText)findViewById(R.id.cPwText));
        pwIcon.setView((ImageView) findViewById(R.id.cPwImage));
        pwIcon.setImageId(ICON_PW);
        pwIcon.setSetOnImage(R.drawable.icon_lock_on);
        pwIcon.setSetOffImage(R.drawable.icon_lock_off);
        imageIcons.add(pwIcon);

        //업체명
        ImageIcon companyNameIcon = new ImageIcon();
        companyNameIcon.setEditText((EditText)findViewById(R.id.cCampanyNameText));
        companyNameIcon.setView((ImageView) findViewById(R.id.cCampanyNameImage));
        companyNameIcon.setImageId(ICON_CNAME);
        companyNameIcon.setSetOnImage(R.drawable.icon_doc_on);
        companyNameIcon.setSetOffImage(R.drawable.icon_doc_off);
        imageIcons.add(companyNameIcon);

        //대표자
        ImageIcon ceoIcon = new ImageIcon();
        ceoIcon.setEditText((EditText)findViewById(R.id.cCeoText));
        ceoIcon.setView((ImageView) findViewById(R.id.cCeoImage));
        ceoIcon.setImageId(ICON_CEO);
        ceoIcon.setSetOnImage(R.drawable.icon_user_on);
        ceoIcon.setSetOffImage(R.drawable.icon_user_off);
        imageIcons.add(ceoIcon);

        //사업자등록번호
        ImageIcon companyIdentityIcon = new ImageIcon();
        companyIdentityIcon.setEditText((EditText)findViewById(R.id.cCompanyIdentityText));
        companyIdentityIcon.setView((ImageView) findViewById(R.id.cCompanyIdentityImage));
        companyIdentityIcon.setImageId(ICON_CIDENTITY);
        companyIdentityIcon.setSetOnImage(R.drawable.icon_doc_on);
        companyIdentityIcon.setSetOffImage(R.drawable.icon_doc_off);
        imageIcons.add(companyIdentityIcon);

        //업종
        ImageIcon typeIcon = new ImageIcon();
        typeIcon.setEditText((EditText)findViewById(R.id.ctypeText));
        typeIcon.setView((ImageView) findViewById(R.id.ctypeImage));
        typeIcon.setImageId(ICON_TYPE);
        typeIcon.setSetOnImage(R.drawable.icon_doc_on);
        typeIcon.setSetOffImage(R.drawable.icon_doc_off);
        imageIcons.add(typeIcon);

        //업태
        ImageIcon businessIcon = new ImageIcon();
        businessIcon.setEditText((EditText)findViewById(R.id.cBusinessText));
        businessIcon.setView((ImageView) findViewById(R.id.cBusinessImage));
        businessIcon.setImageId(ICON_BUSINESS);
        businessIcon.setSetOnImage(R.drawable.icon_doc_on);
        businessIcon.setSetOffImage(R.drawable.icon_doc_off);
        imageIcons.add(businessIcon);

        //사업장 주소
        ImageIcon addressIcon = new ImageIcon();
        addressIcon.setEditText((EditText)findViewById(R.id.cAddressText));
        addressIcon.setView((ImageView) findViewById(R.id.cAddressImage));
        addressIcon.setImageId(ICON_ADRESS);
        addressIcon.setSetOnImage(R.drawable.icon_map_on);
        addressIcon.setSetOffImage(R.drawable.icon_map_off);
        imageIcons.add(addressIcon);

        //휴대폰
        ImageIcon phoneIcon = new ImageIcon();
        phoneIcon.setEditText((EditText)findViewById(R.id.cPhoneText));
        phoneIcon.setView((ImageView) findViewById(R.id.cPhoneImage));
        phoneIcon.setImageId(ICON_PHONE);
        phoneIcon.setSetOnImage(R.drawable.icon_phone_on);
        phoneIcon.setSetOffImage(R.drawable.icon_phone_off);
        imageIcons.add(phoneIcon);

        //인증번호
        ImageIcon phoneCertifyIcon = new ImageIcon();
        phoneCertifyIcon.setEditText((EditText)findViewById(R.id.cCertifyNumberText));
        phoneCertifyIcon.setView((ImageView) findViewById(R.id.cCertifyNumberImage));
        phoneCertifyIcon.setImageId(ICON_PCERTIFY);
        phoneCertifyIcon.setSetOnImage(R.drawable.icon_phone_on);
        phoneCertifyIcon.setSetOffImage(R.drawable.icon_phone_off);
        imageIcons.add(phoneCertifyIcon);
    }

    private void buttonEvent(){
        //취소버튼
        Button cancelbtn = (Button)findViewById(R.id.cCancelbtn);
        cancelbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConfirmDialog confirmDialog = new ConfirmDialog(getCompanyUserRegisterActivity()) {
                    @Override
                    public void onClickConfirm(ConfirmDialog confirmDialog) {
                        confirmDialog.dismiss();
                        getCompanyUserRegisterActivity().finish();
                    }

                    @Override
                    public void onClickCancel() {

                    }
                };
                confirmDialog.getConfirmBtn().setText("확인");
                confirmDialog.getCancelBtn().setText("취소");
                confirmDialog.getTitleText().setText("정말로 취소를 하시겠습니까?");
                confirmDialog.show();
            }
        });
    }

    /* EditText 포커스 바꼈을시 아이콘 이미지 상태 바꿈
   *
   * */
    private void editTextEvent(){

        for(int i=0;i<imageIcons.size();i++) {
            imageIcons.get(i).getEditText().setId(imageIcons.get(i).getImageId());
            imageIcons.get(i).getEditText().setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (hasFocus) {
                        imageIcons.get(v.getId()).getView().setBackgroundResource(imageIcons.get(v.getId()).getSetOnImage());
                    } else {
                        imageIcons.get(v.getId()).getView().setBackgroundResource(imageIcons.get(v.getId()).getSetOffImage());
                    }
                }
            });
        }

    }

    public CompanyUserRegisterActivity getCompanyUserRegisterActivity(){
        return this;
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        RecycleUtils.recursiveRecycle(getWindow().getDecorView());
    }

}
