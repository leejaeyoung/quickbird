package com.quickbird.quickbird;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import java.util.ArrayList;

import Dialog.ConfirmDialog;
import Register.ImageIcon;
import memory.RecycleUtils;

/**
 * Created by KyoungSik on 2017-03-08.
 * 배송자 등록하기
 */
public class DeliveryRegisterActivity extends Activity {

    private final String TAG = "DeliveryRegisterActivity";

    private ArrayList<ImageIcon> imageIcons = new ArrayList<ImageIcon>();//이미지 아이콘 관리

    private boolean drCheck = false;
    private boolean arCheck = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery_register);
        init();
    }

    private void init(){
        imageIconinit();
        buttonEvent();
        editTextEvent();
    }


    private void imageIconinit(){
        //이름
        ImageIcon nameIcon = new ImageIcon();
        nameIcon.setEditText((EditText)findViewById(R.id.drNameText));
        nameIcon.setView((ImageView)findViewById(R.id.drNameImage));
        nameIcon.setSetOnImage(R.drawable.icon_user_on);
        nameIcon.setSetOffImage(R.drawable.icon_user_off);
        imageIcons.add(nameIcon);

        //휴대폰
        ImageIcon phoneIcon = new ImageIcon();
        phoneIcon.setEditText((EditText)findViewById(R.id.drPhoneText));
        phoneIcon.setView((ImageView)findViewById(R.id.drPhoneImage));
        phoneIcon.setSetOnImage(R.drawable.icon_phone_on);
        phoneIcon.setSetOffImage(R.drawable.icon_phone_off);
        imageIcons.add(phoneIcon);

        //운송수단
        ImageIcon tmIcon = new ImageIcon();
        tmIcon.setEditText((EditText)findViewById(R.id.drTransportMeansText));
        tmIcon.setView((ImageView)findViewById(R.id.drTransportMeansImage));
        tmIcon.setSetOnImage(R.drawable.icon_car_on);
        tmIcon.setSetOffImage(R.drawable.icon_car_off);
        imageIcons.add(tmIcon);

        //출발지
        ImageIcon dpIcon = new ImageIcon();
        dpIcon.setEditText((EditText)findViewById(R.id.drDeparturePointText));
        dpIcon.setView((ImageView)findViewById(R.id.drDeparturePointImage));
        dpIcon.setSetOnImage(R.drawable.icon_map_on);
        dpIcon.setSetOffImage(R.drawable.icon_map_off);
        imageIcons.add(dpIcon);

        //도착지
        ImageIcon apIcon = new ImageIcon();
        apIcon.setEditText((EditText)findViewById(R.id.drArrivePointText));
        apIcon.setView((ImageView)findViewById(R.id.drArrivePointImage));
        apIcon.setSetOnImage(R.drawable.icon_map_on);
        apIcon.setSetOffImage(R.drawable.icon_map_off);
        imageIcons.add(apIcon);

        //예상거리
        ImageIcon distanceIcon = new ImageIcon();
        distanceIcon.setEditText((EditText)findViewById(R.id.drDistanceText));
        distanceIcon.setView((ImageView)findViewById(R.id.drDistanceImage));
        distanceIcon.setSetOnImage(R.drawable.icon_map_on);
        distanceIcon.setSetOffImage(R.drawable.icon_map_off);
        imageIcons.add(distanceIcon);

        //추천가격
        ImageIcon recommandIcon = new ImageIcon();
        recommandIcon.setEditText((EditText)findViewById(R.id.drRecommendText));
        recommandIcon.setView((ImageView)findViewById(R.id.drRecommandImage));
        recommandIcon.setSetOnImage(R.drawable.icon_card_on);
        recommandIcon.setSetOffImage(R.drawable.icon_card_off);
        imageIcons.add(recommandIcon);

        //메모
        ImageIcon memoIcon = new ImageIcon();
        memoIcon.setEditText((EditText)findViewById(R.id.drMemoText));
        memoIcon.setView((ImageView)findViewById(R.id.drMemoImage));
        memoIcon.setSetOnImage(R.drawable.icon_pencil_on);
        memoIcon.setSetOffImage(R.drawable.icon_pencil_off);
        imageIcons.add(memoIcon);
    }

    /* 버튼 이벤트
    *
    * */
    private void buttonEvent(){
        Button cancelbtn = (Button)findViewById(R.id.drcancelbtn);
        cancelbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConfirmDialog confirmDialog = new ConfirmDialog(getDeliveryRegisterActivity()) {
                    @Override
                    public void onClickConfirm(ConfirmDialog confirmDialog) {
                        confirmDialog.dismiss();
                        getDeliveryRegisterActivity().finish();
                    }

                    @Override
                    public void onClickCancel() {

                    }
                };
                confirmDialog.getConfirmBtn().setText("확인");
                confirmDialog.getCancelBtn().setText("취소");
                confirmDialog.getTitleText().setText("정말로 취소를 하시겠습니까?");
                confirmDialog.show();
            }
        });
        Button exitbtn = (Button)findViewById(R.id.drexitbtn);
        exitbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        Button drcheckbtn = (Button)findViewById(R.id.drDrCheckboxbtn);
        drcheckbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!drCheck){
                    v.setBackgroundResource(R.drawable.icon_check_on);
                    drCheck = true;
                }else{
                    v.setBackgroundResource(R.drawable.icon_check_off);
                    drCheck = false;
                }
            }
        });

        Button archeckbtn = (Button)findViewById(R.id.drArCheckboxbtn);
        archeckbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!arCheck){
                    v.setBackgroundResource(R.drawable.icon_check_on);
                    arCheck = true;
                }else{
                    v.setBackgroundResource(R.drawable.icon_check_off);
                    arCheck = false;
                }
            }
        });
    }

    /* EditText 포커스 바꼈을시 아이콘 이미지 상태 바꿈
*
* */
    private void editTextEvent(){

        for(int i=0;i<imageIcons.size();i++) {
            imageIcons.get(i).getEditText().setId(i);
            imageIcons.get(i).getEditText().setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (hasFocus) {
                        imageIcons.get(v.getId()).getView().setBackgroundResource(imageIcons.get(v.getId()).getSetOnImage());
                    } else {
                        imageIcons.get(v.getId()).getView().setBackgroundResource(imageIcons.get(v.getId()).getSetOffImage());
                    }
                }
            });
        }
    }

    public DeliveryRegisterActivity getDeliveryRegisterActivity(){
        return this;
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        RecycleUtils.recursiveRecycle(getWindow().getDecorView());
    }

}
