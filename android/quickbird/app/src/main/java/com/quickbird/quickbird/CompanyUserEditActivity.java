package com.quickbird.quickbird;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import java.util.ArrayList;

import Register.ImageIcon;
import memory.RecycleUtils;

/**
 * Created by KyoungSik on 2017-03-13.
 * 사업자 회원 정보수정
 */
public class CompanyUserEditActivity extends Activity {

    private final String TAG = "CompanyUserEditActivity";

    public final int ICON_EMAIL = 0;//이메일 아이디
    public final int ICON_CNAME = 1;//업체명
    public final int ICON_CEO = 2;//대표자
    public final int ICON_TYPE = 3;//업종
    public final int ICON_BUSINESS = 4;//업태
    public final int ICON_ADRESS = 5;//사업장 주소
    public final int ICON_PHONE = 6;//휴대폰

    private ArrayList<ImageIcon> imageIcons = new ArrayList<ImageIcon>();//이미지 아이콘 관리 구조체

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_userinfo_edit);
        init();
    }

    private void init(){
        imageIconinit();
        buttonEvent();
        editTextEvent();
    }

    /* 이미지 아이콘 구조체 초기화
    *
    * */
    private void imageIconinit(){

        //이메일 아이디
        ImageIcon emaildIcon = new ImageIcon();
        emaildIcon.setEditText((EditText)findViewById(R.id.cueEmailidText));
        emaildIcon.setView((ImageView) findViewById(R.id.cueEmailidImage));
        emaildIcon.setImageId(ICON_EMAIL);
        emaildIcon.setSetOnImage(R.drawable.icon_email_on);
        emaildIcon.setSetOffImage(R.drawable.icon_email_off);
        imageIcons.add(emaildIcon);

        //업체명
        ImageIcon companyNameIcon = new ImageIcon();
        companyNameIcon.setEditText((EditText)findViewById(R.id.cueCampanyNameText));
        companyNameIcon.setView((ImageView) findViewById(R.id.cueCampanyNameImage));
        companyNameIcon.setImageId(ICON_CNAME);
        companyNameIcon.setSetOnImage(R.drawable.icon_doc_on);
        companyNameIcon.setSetOffImage(R.drawable.icon_doc_off);
        imageIcons.add(companyNameIcon);

        //대표자
        ImageIcon ceoIcon = new ImageIcon();
        ceoIcon.setEditText((EditText) findViewById(R.id.cueCeoText));
        ceoIcon.setView((ImageView) findViewById(R.id.cueCeoImage));
        ceoIcon.setImageId(ICON_CEO);
        ceoIcon.setSetOnImage(R.drawable.icon_user_on);
        ceoIcon.setSetOffImage(R.drawable.icon_user_off);
        imageIcons.add(ceoIcon);

        //업종
        ImageIcon typeIcon = new ImageIcon();
        typeIcon.setEditText((EditText)findViewById(R.id.cuetypeText));
        typeIcon.setView((ImageView) findViewById(R.id.cuetypeImage));
        typeIcon.setImageId(ICON_TYPE);
        typeIcon.setSetOnImage(R.drawable.icon_doc_on);
        typeIcon.setSetOffImage(R.drawable.icon_doc_off);
        imageIcons.add(typeIcon);

        //업태
        ImageIcon businessIcon = new ImageIcon();
        businessIcon.setEditText((EditText)findViewById(R.id.cueBusinessText));
        businessIcon.setView((ImageView) findViewById(R.id.cueBusinessImage));
        businessIcon.setImageId(ICON_BUSINESS);
        businessIcon.setSetOnImage(R.drawable.icon_doc_on);
        businessIcon.setSetOffImage(R.drawable.icon_doc_off);
        imageIcons.add(businessIcon);

        //사업장 주소
        ImageIcon addressIcon = new ImageIcon();
        addressIcon.setEditText((EditText)findViewById(R.id.cueAddressText));
        addressIcon.setView((ImageView) findViewById(R.id.cueAddressImage));
        addressIcon.setImageId(ICON_ADRESS);
        addressIcon.setSetOnImage(R.drawable.icon_map_on);
        addressIcon.setSetOffImage(R.drawable.icon_map_off);
        imageIcons.add(addressIcon);

        //휴대폰
        ImageIcon phoneIcon = new ImageIcon();
        phoneIcon.setEditText((EditText)findViewById(R.id.cuePhoneText));
        phoneIcon.setView((ImageView) findViewById(R.id.cuePhoneImage));
        phoneIcon.setImageId(ICON_PHONE);
        phoneIcon.setSetOnImage(R.drawable.icon_phone_on);
        phoneIcon.setSetOffImage(R.drawable.icon_phone_off);
        imageIcons.add(phoneIcon);

    }

    /* EditText 포커스 바꼈을시 아이콘 이미지 상태 바꿈
*
* */
    private void editTextEvent(){

        for(int i=0;i<imageIcons.size();i++) {
            imageIcons.get(i).getEditText().setId(imageIcons.get(i).getImageId());
            imageIcons.get(i).getEditText().setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (hasFocus) {
                        imageIcons.get(v.getId()).getView().setBackgroundResource(imageIcons.get(v.getId()).getSetOnImage());
                    } else {
                        imageIcons.get(v.getId()).getView().setBackgroundResource(imageIcons.get(v.getId()).getSetOffImage());
                    }
                }
            });
        }

    }

    /* 버튼 이벤트
    *
    * */
    private void buttonEvent(){
        Button infoeditbtn = (Button)findViewById(R.id.cueinfoEditbtn);
        infoeditbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "정보수정");
            }
        });

        Button cancelbtn = (Button)findViewById(R.id.cueCancelbtn);
        cancelbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "취소");
                finish();
            }
        });
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        RecycleUtils.recursiveRecycle(getWindow().getDecorView());
    }
}
