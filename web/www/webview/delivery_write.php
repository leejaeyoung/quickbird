<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--[if lt IE 9]>
	<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>
	<![endif]-->
	<!--[if lt IE 8]>
	<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE8.js"></script>
	<![endif]-->
	<!--[if lt IE 7]>
	<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE7.js"></script>
	<![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <title>배송자 등록</title>
	<link rel="stylesheet" href="../css/body.css">
	<link rel="stylesheet" href="../css/form.css">
</head>
<body id="wrap">
	<!--s: 프로필사진영역-->
    <div class="f_profile">
    	<div class="f_pic">
        	<img src="../images/profile_man_white.png"><!--프로필사진-->
            <a href="#" class="f_pic_butt"><img src="../images/icon_camera.png"></a><!--카메라버튼-->
        </div>
    </div>
    <!--e: 프로필사진영역-->
    
    <!--s: 작성폼-->
    <div class="f_form">
    	<!--이름-->
        <div class="f_form_d">
            <input type="text" class="bg_name">
            <label>이름</label>
        </div>

        <!--휴대폰-->
        <div class="f_form_d">
        	<input type="text" placeholder="-없이 번호입력" class="bg_mobile">
        	<label>휴대폰</label>
        </div>

        <!--운송수단-->
        <div class="f_form_d">
            <select class="bg_car">
                <option>선택하세요</option>
                <option>오토바이</option>
                <option>승용차</option>
                <option>용달차</option>
                <option>화물차</option>
            </select>	
            <label>운송수단</label>
        </div>
    </div>
    
    <div class="f_form">
    	<!--출발지-->
        <div class="f_form_d">
            <input type="text" class="bg_map">
            <label>출발지</label>
        </div>
        
        <!--도착지-->
        <div class="f_form_d">
            <input type="text" class="bg_map">
            <label>도착지</label>
        </div>
        
        <!--예상거리-->
        <div class="f_form_d">
            <input type="text" class="bg_map">
            <label>예상거리</label>
            <span class="right_text">KM</span>
        </div>
    </div>
    
    <div class="f_form">
    	<!--추천가격-->
    	<div class="f_form_d">
            <input type="text" class="bg_pay">
            <label>추천가격</label>
            <span class="right_text">원</span>
        </div>
    </div>
    
    <div class="f_form">
    	<!--메모-->
        <div class="f_form_d">
    		<textarea wrap="physical" placeholder="메모를 입력하세요" class="bg_memo"></textarea>
        </div>
    </div>
    <!--e: 작성폼--> 
    
</body>
</html>