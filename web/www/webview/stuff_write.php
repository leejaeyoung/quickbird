<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--[if lt IE 9]>
	<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>
	<![endif]-->
	<!--[if lt IE 8]>
	<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE8.js"></script>
	<![endif]-->
	<!--[if lt IE 7]>
	<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE7.js"></script>
	<![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <title>화물 등록</title>
	<link rel="stylesheet" href="../css/body.css">
	<link rel="stylesheet" href="../css/form.css">
</head>
<body>
    <!--s: 화물사진영역-->
    <div class="f_stuff">
        <!--등록한 이미지가 없을때-->
        <a href="#" class="stuff_x">  
            <img src="../images/icon_stuff_x.png"><br>
            <span>화물 사진/동영상 첨부</span>
        </a>
        <!--등록한 이미지가 있을때-->
        <!--<a href="#" class="stuff_o" style="background-image:url(../images/test_img01.png); background-size:cover; background-position:center; "></a>-->
    </div>
    <!--e: 프로필사진영역-->
    
	<div id="wrap" style="padding-top:18px;">
        <!--s: 작성폼-->
        <div class="f_form">
            <!--품목-->
            <div class="f_form_d">
                <input type="text" class="bg_tag">
                <label>품목</label>
            </div>
    
            <!--크기-->
            <div class="f_form_d">
                <input type="text" class="bg_size">
                <label>크기</label>
                <span class="right_text">CM</span>
            </div>
    
            <!--무게-->
            <div class="f_form_d">
                <input type="text" class="bg_weight">
                <label>무게</label>
                <span class="right_text">KG</span>
            </div>
        </div>
        
        <div class="f_form">
            <!--받는분-->
            <div class="f_form_d">
                <input type="text" class="bg_name">
                <label>받는분</label>
            </div>
            
            <!--출발지-->
            <div class="f_form_d">
                <input type="text" class="bg_map">
                <label>출발지</label>
            </div>
            
            <!--도착지-->
            <div class="f_form_d">
                <input type="text" class="bg_map">
                <label>도착지</label>
            </div>
            
            <!--예상거리-->
            <div class="f_form_d">
                <input type="text" class="bg_map">
                <label>예상거리</label>
                <span class="right_text">KM</span>
            </div>
        </div>
        
        <div class="f_form">
            <!--추천가격-->
            <div class="f_form_d">
                <input type="text" class="bg_pay">
                <label>추천가격</label>
                <span class="right_text">원</span>
            </div>
        </div>
        
        <div class="f_form">
            <!--메모-->
            <div class="f_form_d">
                <textarea wrap="physical" placeholder="메모를 입력하세요" class="bg_memo"></textarea>
            </div>
        </div>
        <!--e: 작성폼--> 
    </div>
</body>
</html>