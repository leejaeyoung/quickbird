<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--[if lt IE 9]>
	<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>
	<![endif]-->
	<!--[if lt IE 8]>
	<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE8.js"></script>
	<![endif]-->
	<!--[if lt IE 7]>
	<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE7.js"></script>
	<![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <title>배송자 상세보기</title>
	<link rel="stylesheet" href="../css/body.css">
	<link rel="stylesheet" href="../css/form.css">
</head>
<body>
    <!--s: 프로필영역-->
    <div class="f_profile_d">
        <div class="profile_img" style="background-image:url(../images/profile_man_white.png); background-size:cover; background-position:center;">
        </div>
        <div class="profile_info">
        	<h2>홍길동</h2>
            <a href="#" class="profile_call"><img src="../images/icon_phone.png">010.1234.5678</a>
            <p class="star"><img src="../images/icon_star.png"><img src="../images/icon_star.png"><img src="../images/icon_star.png"><img src="../images/icon_star.png"><img src="../images/icon_star2.png"><span>(4점)</span></p>
        </div>
        <span class="date">17.03.03</span>
    </div>
    <!--e: 프로필영역-->
    
	<div id="wrap" style="padding-top:18px; padding-bottom:20px;">      
        <!--s: 작성폼-->
        <div class="f_form">
            <!--운송수단-->
            <div class="f_form_d">
                <input type="text" class="bg_car_d" placeholder="ex) 오토바이">
                <label>운송수단</label>
            </div>
        </div>
        
        <div class="f_form">

            <!--출발지-->
            <div class="f_form_d">
                <input type="text" class="bg_map_d" placeholder="ex) 부산 사상구 모라2동">
                <label>출발지</label>
            </div>
            
            <!--도착지-->
            <div class="f_form_d">
                <input type="text" class="bg_map_d" placeholder="ex) 부산 해운대구 장산1동">
                <label>도착지</label>
            </div>
            
            <!--예상거리-->
            <div class="f_form_d">
                <input type="text" class="bg_map_d" placeholder="ex) 2.5">
                <label>예상거리</label>
                <span class="right_text">KM</span>
            </div>
        </div>
        
        <div class="f_form">
            <!--추천가격-->
            <div class="f_form_d">
                <input type="text" class="bg_pay_d" placeholder="ex) 20,000">
                <label>추천가격</label>
                <span class="right_text">원</span>
            </div>
        </div>
        
        <div class="f_form">
            <!--메모-->
            <div class="f_form_d">
                <textarea class="bg_memo_d" wrap="physical" placeholder="메모를 입력하세요"></textarea>
            </div>
        </div>
        <!--e: 작성폼--> 
    </div>
    
    <!--s: 후기-->
    <div class="f_reply">
        <div class="f_reply_tit">
            후 기<span>(2)</span>
        </div>
        
        <!--댓글-->
        <div class="f_reply_con">
            <div class="pic" style="background-image:url(../images/profile_man_white.png); background-size:cover; background-position:center;"></div>
            <div class="info">
                <p><img src="../images/icon_star.png"><img src="../images/icon_star.png"><img src="../images/icon_star.png"><img src="../images/icon_star.png"><img src="../images/icon_star2.png"></p>
                <p>배송자님께서 정말 친절하고 빠르게 화물을 운송해주셨어요! 감사합니다. </p>
            </div>
            <span class="date">17.03.03</span>
        </div>
        
        <!--댓글-->
        <div class="f_reply_con">
            <div class="pic" style="background-image:url(../images/profile_man_white.png); background-size:cover; background-position:center;"></div>
            <div class="info">
                <p><img src="../images/icon_star.png"><img src="../images/icon_star.png"><img src="../images/icon_star.png"><img src="../images/icon_star.png"><img src="../images/icon_star2.png"></p>
                <p>배송자님께서 정말 친절하고 빠르게 화물을 운송해주셨어요! 감사합니다. </p>
            </div>
            <span class="date">17.03.03</span>
        </div>
        
    </div>
    <!--e: 후기-->
</body>
</html>