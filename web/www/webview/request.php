<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <title>나에게 요청한 화물</title>
	<link rel="stylesheet" href="../css/body.css">
	<link rel="stylesheet" href="../css/home.css">
</head>
<body id="wrap">
    <!--s: 배송대기중-->
	<div class="h_list_box">
		<!--이미지-->
		<div class="img">
			<img src="../images/profile_stuff.png">
		</div>
		<!--정보-->
		<div class="info">
			<h2>전자제품 <img src="../images/icon_error.png"></h2>
			<p class="place">
            	<img src="../images/icon_list_map_blue.png">부산 좌2동 → 
                <img src="../images/icon_list_map_blue.png">서울 서초동</p>
            <p><span>크기</span>80x40x50 <span>무게</span>1.5kg</p>
		</div>
        <!--날짜-->
        <div class="date_m">17.03.03</div>
        <!--더보기버튼-->
        <div class="more_m"><img src="../images/icon_more.png"></div>
	</div>
	<!--e: 배송대기중 -->
    
    <!--s: 배송중-->
	<div class="h_list_box">
		<!--이미지-->
		<div class="img">
			<img src="../images/profile_stuff.png">
		</div>
		<!--정보-->
		<div class="info">
			<h2>전자제품 <!--<img src="../images/icon_error.png">--></h2>
			<p class="place">
            	<img src="../images/icon_list_map_blue.png">부산 좌2동 → 
                <img src="../images/icon_list_map_blue.png">서울 서초동</p>
            <p><span>크기</span>80x40x50 <span>무게</span>1.5kg</p>
		</div>
        <!--날짜-->
        <div class="date_m">17.03.03</div>
        <!--더보기버튼-->
        <div class="more_m"><img src="../images/icon_more.png"></div>
	</div>
	<!--e: 배송중 -->
    
    <!--s: 배송완료-->
	<div class="h_list_box">
		<!--이미지-->
		<div class="img">
			<img src="../images/profile_stuff.png">
		</div>
		<!--정보-->
		<div class="info">
			<h2>전자제품 <img src="../images/icon_error.png"></h2>
			<p class="place">
            	<img src="../images/icon_list_map_blue.png">부산 좌2동 → 
                <img src="../images/icon_list_map_blue.png">서울 서초동</p>
            <p><span>크기</span>80x40x50 <span>무게</span>1.5kg</p>
		</div>
        <!--날짜-->
        <div class="date_m">17.03.03</div>
        <!--더보기버튼-->
        <div class="more_m"><img src="../images/icon_more.png"></div>
	</div>
	<!--e: 배송완료 -->
    

    
</body>
</html>