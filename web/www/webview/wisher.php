<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--[if lt IE 9]>
	<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>
	<![endif]-->
	<!--[if lt IE 8]>
	<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE8.js"></script>
	<![endif]-->
	<!--[if lt IE 7]>
	<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE7.js"></script>
	<![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <title>배송희망자</title>
	<link rel="stylesheet" href="../css/body.css">
	<link rel="stylesheet" href="../css/home.css">
</head>
<body id="wrap">
	<!--s: 오렌지색 배송자리스트-->
	<div class="h_list_box orange">
		<!--이미지-->
		<div class="img">
			<img src="../images/profile_man.png">
		</div>
		<!--정보-->
		<div class="info">
			<h2>김영철 <span><img src="../images/icon_star.png"><img src="../images/icon_star.png"><img src="../images/icon_star.png"><img src="../images/icon_star2.png"><img src="../images/icon_star2.png"> (4점)</span></h2>
			<p class="place text_w">
            	<img src="../images/icon_car.png" style="width:10.5px; margin-bottom:1px;">오토바이<span style="margin-right:4px;"></span><!--여백-->
                <img src="../images/icon_phone.png" style="width:6.5px; margin-bottom:0px;">010.1234.5678
            </p>
            <p><span>등록일</span>17.03.03</p>
		</div>
        <!--더보기-->
        <div class="more_m"><img src="../images/icon_more.png"></div>
		<!--태그-->
		<div class="tag_l">
        	<img src="../images/tag_check.png">
        </div>
	</div>
	<!--e: 오렌지색 배송자리스트-->
    
    <!--s: 오렌지색 배송자리스트-->
	<div class="h_list_box orange">
		<!--이미지-->
		<div class="img">
			<img src="../images/profile_man.png">
		</div>
		<!--정보-->
		<div class="info">
			<h2>김영철 <span><img src="../images/icon_star.png"><img src="../images/icon_star.png"><img src="../images/icon_star.png"><img src="../images/icon_star2.png"><img src="../images/icon_star2.png"> (4점)</span></h2>
			<p class="place text_w">
            	<img src="../images/icon_car.png" style="width:10.5px; margin-bottom:1px;">오토바이<span style="margin-right:4px;"></span><!--여백-->
                <img src="../images/icon_phone.png" style="width:6.5px; margin-bottom:0px;">010.1234.5678
            </p>
            <p><span>등록일</span>17.03.03</p>
		</div>
        <!--더보기-->
        <div class="more_m"><img src="../images/icon_more.png"></div>
		<!--태그-->
		<!--<div class="tag_l">
        	<img src="../images/tag_check.png">
        </div>-->
	</div>
	<!--e: 오렌지색 배송자리스트-->
    
    <!--s: 오렌지색 배송자리스트-->
	<div class="h_list_box orange">
		<!--이미지-->
		<div class="img">
			<img src="../images/profile_man.png">
		</div>
		<!--정보-->
		<div class="info">
			<h2>김영철 <span><img src="../images/icon_star.png"><img src="../images/icon_star.png"><img src="../images/icon_star.png"><img src="../images/icon_star2.png"><img src="../images/icon_star2.png"> (4점)</span></h2>
			<p class="place text_w">
            	<img src="../images/icon_car.png" style="width:10.5px; margin-bottom:1px;">오토바이<span style="margin-right:4px;"></span><!--여백-->
                <img src="../images/icon_phone.png" style="width:6.5px; margin-bottom:0px;">010.1234.5678
            </p>
            <p><span>등록일</span>17.03.03</p>
		</div>
        <!--더보기-->
        <div class="more_m"><img src="../images/icon_more.png"></div>
		<!--태그-->
		<!--<div class="tag_l">
        	<img src="../images/tag_check.png">
        </div>-->
	</div>
	<!--e: 오렌지색 배송자리스트-->
    
    
</body>
</html>