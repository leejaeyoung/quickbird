<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <title>Home</title>
	<link rel="stylesheet" href="../css/body.css">
	<link rel="stylesheet" href="../css/home.css">
	<!--<script src="../js/jquery-1.12.4.min.js"></script>
	<script type="text/javascript" src="../js/class.js"></script>
	<script src="../bootstrap-3.3.2-dist/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="../bootstrap-3.3.2-dist/css/bootstrap.min.css">
	<script type="text/javascript" src="../js/newlist.js"></script>-->
</head>
<body id="wrap">
    <!--s: 파란색 화물리스트-->
	<div class="h_list_box">
		<!--이미지-->
		<div class="img">
			<img src="../images/profile_stuff.png">
		</div>
		<!--정보-->
		<div class="info">
			<h2>전자제품 <img src="../images/icon_error.png"></h2>
			<p class="place">
            	<img src="../images/icon_list_map_blue.png">부산 좌2동 → 
                <img src="../images/icon_list_map_blue.png">서울 서초동</p>
            <p><span>크기</span>80x40x50 <span>무게</span>1.5kg</p>
		</div>
        <!--날짜-->
        <div class="date">17.03.03</div>
		<!--태그-->
		<div class="tag">
        	<img src="../images/tag_home01.png">
        </div>
	</div>
	<!--e: 파란색 화물리스트-->


	<!--s: 오렌지색 배송자리스트-->
	<div class="h_list_box orange">
		<!--이미지-->
		<div class="img">
			<img src="../images/profile_man.png">
		</div>
		<!--정보-->
		<div class="info">
			<h2>이상진</h2>
			<p class="place font_orange">
            	<img src="../images/icon_list_map_orange.png">부산 좌2동 → 
                <img src="../images/icon_list_map_orange.png">서울 서초동
            </p>
            <p><span>운송수단</span>오토바이</p>
		</div>
        <!--날짜-->
        <div class="date">17.03.03</div>
		<!--태그-->
		<div class="tag">
        	<img src="../images/tag_home02.png">
        </div>
	</div>
	<!--e: 오렌지색 배송자리스트-->
    
    <!--s: 파란색 화물리스트-->
	<div class="h_list_box">
		<!--이미지-->
		<div class="img">
			<img src="../images/profile_stuff.png">
		</div>
		<!--정보-->
		<div class="info">
			<h2>전자제품</h2>
			<p class="place">
            	<img src="../images/icon_list_map_blue.png">부산 좌2동 → 
                <img src="../images/icon_list_map_blue.png">서울 서초동</p>
            <p><span>크기</span>80x40x50 <span>무게</span>1.5kg</p>
		</div>
        <!--날짜-->
        <div class="date">17.03.03</div>
		<!--태그-->
		<div class="tag">
        	<img src="../images/tag_home01.png">
        </div>
	</div>
	<!--e: 파란색 화물리스트-->
</body>
</html>